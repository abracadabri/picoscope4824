# -*- coding: utf-8 -*-
"""
Created on Sun Nov 09 11:00:38 2014

@author: Helene
"""

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from ctypes import byref, POINTER, create_string_buffer, c_float, \
    c_int16, c_int32, c_uint32, c_void_p, CFUNCTYPE

import py_compile

from collections import deque

import cPickle


import readconfigfile
import time
import numpy as np
import ps4000a
try:
    from PyQt4.QtCore import QString
except ImportError:
    QString = str

try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


class DataTaker(QThread):
    
       
    def __init__(self, lock,instrument, n_active_channels, parent=None):
        print "Pico created"
        super(DataTaker, self).__init__(parent)

        """
        COMMENT: here instrument will be from the class picoscope
        """
        self.instrument= instrument
        #self.nsamples= nsamples
        self.n_active_channels= n_active_channels
        self.lock = lock
        self.stopped = True
        self.paused = False
        self.mutex = QMutex()
        
        self.completed = False
        self.debug = readconfigfile.get_debug_setting()
  
        
        self.script_file_name=readconfigfile.get_script_name()

        
    
    def __del__(self):
        print "Pico deleted"

 
    def set_acquisition_parameters(self,dt,sampling_interval, nsamples):
        self.observation_duration=dt
        self.nsamples=nsamples
        self.sampling_interval=sampling_interval
 
    def run(self):
        print "Pico begin run"
        self.stopped = False
    
        script = open(self.script_file_name)

        print "opened script "+self.script_file_name 
        exec (script)

        script.close()   
        self.completed=True
        self.emit(SIGNAL("script_finished(bool)"), self.completed)        
        self.stopped = True        

        print "Pico run is over"    
        
        
    def stop(self):
        try:
            self.mutex.lock()
            self.stopped = True
            print "Pico stopped"
        finally:
            self.mutex.unlock()

    def pause(self):
        print "Pico paused"
        self.paused = True
            
    def resume(self,display=True):
        if display:
            print "Pico resumed"
        self.paused = False

    def isPaused(self): 
        return self.paused
            
    def isStopped(self): 
        return self.stopped
            
            
    def check_stopped_or_paused(self):
        while True:            
            if (not self.paused) or self.stopped:
                return self.stopped
            time.sleep(0.1)
    

    
    def read_data(self):
        """
            Call the method "measure" for each instrument in InstrumentHub.
            It collect the different values of corresponding parameters and
            emit a signal which will be catch by other instance for further
            treatment.
        """            

        self.stopped=False

        print(self.nsamples)
        self.data_buffer=(np.empty(self.nsamples, dtype=np.int16))
        data_set=[]
        
        if self.debug==False:
                """
                pretrig=0
                downSampleRatio=1
                downSampleMode=0
                segmentIndex=0
                print("Here")
                
                self.instrument._lowLevelSetDataBuffer(0, self.data_buffer, downSampleMode, segmentIndex)
                self.instrument._lowLevelSetDataBuffer(1, self.data_buffer, downSampleMode, segmentIndex)
                print("Tjhere")
                sampleInterval=1
                
                self.instrument._lowLevelRunStreaming(sampleInterval, 3, int(self.nsamples * pretrig),
                              int(self.nsamples * (1 - pretrig)), downSampleRatio,
                           downSampleMode, len(self.data_buffer))
                           
                ps4000aStreamingReady = CFUNCTYPE(c_void_p,c_uint32, c_uint32, c_int16, c_uint32, c_int16,
                    c_int16, c_void_p)    
                 
                data_buffer_app=[] #deque(maxlen=1000000)
                def CallBackStreaming(noSamples,startIndex,overflow,triggerAt, triggered, autoStop,
                      pParameter):        
#                          t0 = time.clock()
                          #print("Inside the callback")
                          dataV=(self.instrument.rawToV(0,self.data_buffer))
                          #data_buffer_app.append(dataV)
                          self.emit(SIGNAL("data(PyQt_PyObject)"), dataV)
                         
                          #print time.clock() -t0
                          
                cmp_func= ps4000aStreamingReady(CallBackStreaming)
             
                while self.stopped==False:
                    
                    t0= time.clock()
                    time.sleep(1)
                        
                    self.instrument.lib.ps4000aGetStreamingLatestValues(c_int16(self.instrument.handle),
                                                 cmp_func, c_void_p())
                    #print time.clock() - t0
                    
                self.instrument.stop()    
                #self._lowLevelGetValuesAsync(nSamples, 0, 2, 1,0)
    
                print("this is after the function lowLevelGetStreamingLatestValue") 
                for i in range(8):
                        self.instrument._lowLevelClearDataBuffer(i, 0)
                #self.instrument.StreamingHandler()
                """            
                self.instrument.runBlock()                
                self.instrument.waitReady()
                #print("Here we go")
                #print(self.instrument.get_NSamples())
                for i in range(self.n_active_channels):
                    data_set.append(self.instrument.getDataV(i, self.nsamples,returnOverflow=False))
                    
            
                #print(data_set)
                   
                #for i in range(self.instrument.num_channels):
                 #   if self.instrument.acq_mode== 'block':
                  #      data_set.append(self.instrument.getDataV(i, self.instrument.sample_no))       
                   # elif self.instrument.acq_mode=='streaming':
                    #    data_set.append(self.instrument.getDataV_streaming(i, self.instrument.sample_no))
              
            #else:
             #   data_set = []                
        else:
            #data_set=self.instrument.getDataRaw()
        #send data back to the mother ship as an array of floats, but only
#            while self.stopped==False:
            filename="best.rawdat"
            data_set = np.loadtxt(filename,skiprows=1)
            t0= time.clock()
            #data=np.ones((500,7))*t0
            self.emit(SIGNAL("data(PyQt_PyObject)"),data_set)
        """
       # np.array transforme un array en array numpy qui est mieux pour selectionner les colonnes et faire
       # des operations
        """
        self.emit(SIGNAL("data(PyQt_PyObject)"), np.array(data_set))