﻿/**************************************************************************
*
* Filename:    Block_capture.cs
*
* Copyright:   Pico Technology Limited 2014
*
* Author:      KPV
*
* Description:
*   This is a Blocck Capture program that
*   that shows how to trigger off one channel and display results
*
* Examples:
*    Collect a block of samples immediately
*    Collect a block of samples when a trigger event occurs
*
*
* History:
*     11Feb14	KPV	Created
*
* Revision Info: "file %n date %f revision %v"
*						""
*
***************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PS4000aBlockCaptureGui
{
    public partial class BlockExample : Form
    {
        public BlockExample()
        {
            InitializeComponent();
        }

        bool _ready = false;
        public const int BUFFER_SIZE = 1024;
        int status = 0;
        int timeInterval;
        ushort[] inputRanges = { 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000 };
        short handle = 0;
        uint _timebase = 8;
        Imports.Range[] combobox_values = new Imports.Range[8];

        void BlockCallback(short handle, short status, IntPtr pVoid)
        {
            // flag to say done reading data
            _ready = true;
        }

        int adc_to_mv(int raw, Imports.Range range)
        {
            return (raw * inputRanges[(int)(range)]) / Imports.MaxValue;
        }

        short mv_to_adc(int mv, Imports.Range range)
        {
            return (short)((mv * Imports.MaxValue) / inputRanges[(int)(range)]);
        }

        private void BlockExample_Close(object sender, FormClosedEventArgs e)
        {
            Imports.CloseUnit(handle);
        }

        //opens device, brings up the voltage setting panel and populating device information
        private void start_Click(object sender, EventArgs e)
        {
            status = Imports.OpenUnit(out handle, null);

            if (handle == 0)
            {
                MessageBox.Show("The Device code not open\n error code : " + status.ToString(), "Device Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }

            //if not usb3 will need to set up systrem to get  the opwer from usb 2.0
            if (status != 0)
            {
                MessageBox.Show("Using USB power", "Under Powered", MessageBoxButtons.OK, MessageBoxIcon.Information);
                status = Imports.ps4000aChangePowerSource(handle, status);
            }

            Voltset.Visible = true;
            ChannelA_volt.SelectedIndex = 0;
            ChannelB_volt.SelectedIndex = 0;
            ChannelC_volt.SelectedIndex = 0;
            ChannelD_volt.SelectedIndex = 0;
            ChannelE_volt.SelectedIndex = 0;
            ChannelF_volt.SelectedIndex = 0;
            ChannelG_volt.SelectedIndex = 0;
            ChannelH_volt.SelectedIndex = 0;

            string[] description = {
                           "Driver Version    ",
                           "USB Version       ",
                           "Hardware Version  ",
                           "Variant Info      ",
                           "Serial            ",
                           "Cal Date          ",
                           "Kernel Ver        ",
                           "Digital Hardware  ",
                           "Analogue Hardware "
                         };

            System.Text.StringBuilder line = new System.Text.StringBuilder(80);
            for (int i = 0; i < 9; i++)
            {
                short requiredSize;
                Imports.GetUnitInfo(handle, line, 80, out requiredSize, i);

                DeviceInfo.Text += description[i].ToString() + " : " + line.ToString() + Environment.NewLine;
            }
        }

        //Sets up the volatge levels for the channels
        private void volt_next_Click(object sender, EventArgs e)
        {
            //range is shifted by -1 bcause of index list number
            combobox_values[0] = (Imports.Range)(ChannelA_volt.SelectedIndex - 1);
            combobox_values[1] = (Imports.Range)(ChannelB_volt.SelectedIndex - 1);
            combobox_values[2] = (Imports.Range)(ChannelC_volt.SelectedIndex - 1);
            combobox_values[3] = (Imports.Range)(ChannelD_volt.SelectedIndex - 1);
            combobox_values[4] = (Imports.Range)(ChannelE_volt.SelectedIndex - 1);
            combobox_values[5] = (Imports.Range)(ChannelF_volt.SelectedIndex - 1);
            combobox_values[6] = (Imports.Range)(ChannelG_volt.SelectedIndex - 1);
            combobox_values[7] = (Imports.Range)(ChannelH_volt.SelectedIndex - 1);

            //check to make sure that not all channels are off
            if (combobox_values.All(item => item.Equals((Imports.Range)(-1))))
            {
                MessageBox.Show("One channel needs to be enabled", "Error Channel Selection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            for (int ch = 0; ch < 8; ch++)
            {
                 status = Imports.SetChannel(handle,                                         //the device handle
                                            Imports.Channel.CHANNEL_A + ch,                  // the channel you wish to set
                                            (short)(combobox_values[ch] != (Imports.Range)(-1) ? 1 : 0),                          // is enabled if off is not selected
                                            (short)1,                                       // always DC coupled
                                            combobox_values[ch] != (Imports.Range)(-1) ? combobox_values[ch] : (Imports.Range)(0),                            // -1 is not a correct range will cause an error
                                             0); // no analogue offset 

                 if (combobox_values[ch] != (Imports.Range)(-1))
                 {
                     results_chart.Series[((char)(ch + 65)).ToString()].Enabled = true;
                 }
                 else
                 {
                     results_chart.Series[((char)(ch + 65)).ToString()].Enabled = false;
                 }


                if (status != 0)
                {
                    MessageBox.Show("Error setting channel Error Code : " + status.ToString(),
                                    "Error Setting Channel",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                    return;
                }
            }

            Timebase.Visible = true;
        }

        //finds the most suitable time base then saves it as _timebase
        private void timebase_next_Click(object sender, EventArgs e)
        {
            int maxsamples;

            try
            {
                _timebase = uint.Parse(timebasevar.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("please enter numeric value only", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            while (Imports.GetTimebase(handle, _timebase, BUFFER_SIZE, out timeInterval, out maxsamples, 0) != 0)
            {
                _timebase++;
            }

            var result = MessageBox.Show("Time base choosen is: " + _timebase.ToString() + " Which gives : " + timeInterval.ToString() + "ns sample interval\n Select okay if you find this suitable",
                                         "Timebase Settings",
                                         MessageBoxButtons.OKCancel,
                                         MessageBoxIcon.Question);

            if (result == DialogResult.OK)
            {
                TriggerSettings.Visible = true;
                Channel.SelectedIndex = 0;
                direction.SelectedIndex = 0;
            }
        }

        //enables the trigger control if user wishes to use trigger or not
        private void Trigger_enable_CheckedChanged(object sender, EventArgs e)
        {
            Channel_text.Enabled= Trigger_enable.Checked;
            Channel.Enabled = Trigger_enable.Checked;
            threshold_text.Enabled = Trigger_enable.Checked;
            threshold.Enabled = Trigger_enable.Checked;
            direction_text.Enabled = Trigger_enable.Checked;
            direction.Enabled = Trigger_enable.Checked;
        }

        //enables or disables the trigger depending on the check box
        private void trig_next_Click(object sender, EventArgs e)
        {
            short _threshold = 0;

            if (!Trigger_enable.Checked)
            {
                if ((status = Imports.SetSimpleTrigger(handle, 0, Imports.Channel.CHANNEL_A, 0, Imports.ThresholdDirection.Rising, 0, 0)) != 0)// disabling trigger
                {
                    MessageBox.Show("Cannot set trigger, Error code :" + status.ToString(),
                                    "Error Setting Trigger",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
            }
            else
            {
                if (combobox_values[Channel.SelectedIndex] == (Imports.Range)(-1))
                {
                    MessageBox.Show("Please Select a Channel which is enabled");
                    return;
                }

                try
                {
                    _threshold = Int16.Parse(threshold.Text);
                }
                catch (FormatException)
                {
                    MessageBox.Show("please enter numeric value only for threshold", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                status = Imports.SetSimpleTrigger(handle,                                                                    // handle selects the device
                                            1,                                                                      // if this value is non zero will enable trigger
                                            Imports.Channel.CHANNEL_A + Channel.SelectedIndex,                       // select channel
                                            mv_to_adc(_threshold, combobox_values[Channel.SelectedIndex]),          // function use adc values, so must be converted
                                            Imports.ThresholdDirection.Above + direction.SelectedIndex,             // Direction that trigger will see e.g rising or above
                                            0,                                                                      //  Delay time ins sample period between trigger and first sample being taken
                                            0);                                                                     //  time is ms that system will wait if no trigger occurs, set to zero to wait indefinately
            }
            start_capture.Visible = true;
        }

        //will start the actual run block function
        private void start_cap_Click(object sender, EventArgs e)
        {
            Imports.ps4000aBlockReady _callbackDelegate;
            uint sampleCount = BUFFER_SIZE;


            PinnedArray<short>[] minPinned = new PinnedArray<short>[8];
            PinnedArray<short>[] maxPinned = new PinnedArray<short>[8];
            int timeIndisposed;

            for (int i = 0; i < 8; i++)
            {
                short[] minBuffers = new short[sampleCount];
                short[] maxBuffers = new short[sampleCount];
                minPinned[i] = new PinnedArray<short>(minBuffers);
                maxPinned[i] = new PinnedArray<short>(maxBuffers);
                status = Imports.SetDataBuffers(handle, (Imports.Channel)i, maxBuffers, minBuffers, (int)sampleCount, 0, Imports.DownSamplingMode.None);
            }

            _ready = false;
            _callbackDelegate = BlockCallback;
            status = Imports.RunBlock(handle, 0, (int)sampleCount, _timebase, out timeIndisposed, 0, _callbackDelegate, IntPtr.Zero);

            if (status != 0)
            {
                MessageBox.Show("Error running block :" + status.ToString(),
                                    "Error runblock",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                return;
            }
            results.Visible = true;


            while (!_ready)
            {
                Thread.Sleep(0);
            }

            if (_ready)
            {

                short overflow;
                Imports.GetValues(handle, 0, ref sampleCount, 1, Imports.DownSamplingMode.None, 0, out overflow);

                for (int i = 0; i < sampleCount; i++)
                {
                    for (int ch = 0; ch < 8; ch++)
                    {
                        if (combobox_values[ch] != (Imports.Range)(-1))
                        {

                            double time = timeInterval * i;
                            int value = adc_to_mv(maxPinned[ch].Target[i], combobox_values[ch]);
                            results_chart.Series[((char)(ch + 65)).ToString()].Points.AddY(value);
                            results_chart.Series[((char)(ch + 65)).ToString()].Points[i].AxisLabel = time.ToString();
                            switch (ch)
                            {
                                case 0:
                                    ChannelA.Text += (value.ToString() + Environment.NewLine);
                                    break;
                                case 1:
                                    ChannelB.Text += (value.ToString() + Environment.NewLine);
                                    break;
                                case 2:
                                    ChannelC.Text += (value.ToString() + Environment.NewLine);
                                    break;
                                case 3:
                                    ChannelD.Text += (value.ToString() + Environment.NewLine);
                                    break;
                                case 4:
                                    ChannelE.Text += (value.ToString() + Environment.NewLine);
                                    break;
                                case 5:
                                    ChannelF.Text += (value.ToString() + Environment.NewLine);
                                    break;
                                case 6:
                                    ChannelG.Text += (value.ToString() + Environment.NewLine);
                                    break;
                                case 7:
                                    ChannelH.Text += (value.ToString() + Environment.NewLine);
                                    break;
                            }
                        }
                        else
                        {

                        }
                    }
                }

            }
            else
            {
                MessageBox.Show("data collection aborted", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        //returns to the previous pannel
        private void timebase_previous_Click(object sender, EventArgs e)
        {
            Timebase.Visible = true;
        }
        private void trig_previous_Click(object sender, EventArgs e)
        {
            TriggerSettings.Visible = false;
        }
        private void capture_previous_Click(object sender, EventArgs e)
        {
            start_capture.Visible = false;
        }

        private void Restart_Click(object sender, EventArgs e)
        {
            results.Visible = false;
            Timebase.Visible = false;
            start_capture.Visible = false;
            TriggerSettings.Visible = false;

            ChannelA.Text = string.Empty;
            ChannelB.Text = string.Empty;
            ChannelC.Text = string.Empty;
            ChannelD.Text = string.Empty;
            ChannelE.Text = string.Empty;
            ChannelF.Text = string.Empty;
            ChannelG.Text = string.Empty;
            ChannelH.Text = string.Empty;

            foreach (var series in results_chart.Series)
            {
                series.Points.Clear();
            }
        }

        //changes the view so can see it as a graph or just lists of variables
        private void display_change_Click(object sender, EventArgs e)
        {
                chart.Visible = true;
        }

        private void tables_Click(object sender, EventArgs e)
        {
            chart.Visible = false;
        }
    }
}
