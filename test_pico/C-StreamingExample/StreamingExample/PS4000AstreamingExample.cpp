/**************************************************************************
*
* Filename:    PS4000AstreamingExample.c
*
* Copyright:   Pico Technology Limited 2014
*
* Authors:      KPV
*
* Description:
* This program lets you stream data immediately or with a trigger
*
* Examples:
*	Setting Up channels
*	Collect stream data immediately
*	Collect stream data with trigger
*	Setting up trigger using simple trigger
*	Setting up a trigger using individual trigger calls
***************************************************************************/

#include <stdio.h>
#include "windows.h"
#include <conio.h>
#include "../ps4000aApi.h"

#define ChannelCount 8

const uint32_t bufferLength = 100000;
PICO_STATUS status = PICO_OK;
__int64 g_totalSamples = 0;
short    		g_ready = FALSE;
int32_t      		g_sampleCount = 0;
uint32_t g_startIndex;
int16_t        g_autoStop;
int16_t		g_trig = 0;
uint32_t	g_trigAt = 0;

int16_t * buffers[ChannelCount * 2];
int16_t * appBuffers[ChannelCount * 2];
uint32_t timebase = 8;

typedef struct
{
	int16_t DCcoupled;
	int16_t range;
	int16_t enabled;
	float analogueOffset;
}CHANNEL_SETTINGS;

typedef struct
{
	int16_t						handle;
	PS4000A_RANGE				firstRange;
	PS4000A_RANGE				lastRange;
	int16_t						maxADCValue;
	int16_t						ETS;
	CHANNEL_SETTINGS			channelSettings[ChannelCount];
}UNIT;

typedef struct tBufferInfo
{
	UNIT *unit;
	int16_t **driverBuffers;
	int16_t **appBuffers;

} BUFFER_INFO;

uint32_t inputRanges[] = {
	10,
	20,
	50,
	100,
	200,
	500,
	1000,
	2000,
	5000,
	10000,
	20000,
	50000,
	100000,
	200000 };


/****************************************************************************
* adc_to_mv
*
* Convert an 16-bit ADC count into millivolts
****************************************************************************/
int adc_to_mv(long raw, int rangeIndex, UNIT * unit)
{
	return (raw * inputRanges[rangeIndex]) / unit->maxADCValue;
}

/****************************************************************************
* mv_to_adc
*
* Convert a millivolt value into a 16-bit ADC count
*
*  (useful for setting trigger thresholds)
****************************************************************************/
short mv_to_adc(short mv, short rangeIndex, UNIT * unit)
{
	return (mv * unit->maxADCValue) / inputRanges[rangeIndex];
}

/****************************************************************************
* Callback
* used by PS4000 data streaimng collection calls, on receipt of data.
* used to set global flags etc checked by user routines
****************************************************************************/
void PREF4 CallBackStreaming
(
int16_t handle,
int32_t noOfSamples,
uint32_t startIndex,
int16_t overflow,
uint32_t triggerAt,
int16_t triggered,
int16_t autoStop,
void * pParameter
)
{
	BUFFER_INFO * bufferInfo = NULL;

	if (pParameter != NULL)
	{
		bufferInfo = (BUFFER_INFO *)pParameter;
	}
	// used for streaming
	g_sampleCount = noOfSamples;
	g_startIndex = startIndex;
	g_autoStop = autoStop;

	// flag to say done reading data
	g_ready = TRUE;

	// flags to show if & where a trigger has occurred
	g_trig = triggered;
	g_trigAt = triggerAt;



	if (bufferInfo != NULL && noOfSamples)
	{
		for (int channel = 0; channel < ChannelCount; channel++)
		{
			if (bufferInfo->unit->channelSettings[channel].enabled)
			{
				if (appBuffers && buffers)
				{
					if (appBuffers[channel * 2] && buffers[channel * 2])
					{
						memcpy_s(&appBuffers[channel * 2][0], noOfSamples * sizeof(int16_t),
							&buffers[channel * 2][startIndex], noOfSamples * sizeof(int16_t));
					}

					// Min buffers
					if (bufferInfo->appBuffers[channel * 2 + 1] && bufferInfo->driverBuffers[1])
					{
						memcpy_s(&appBuffers[channel * 2 + 1][0], noOfSamples * sizeof(int16_t),
							&buffers[channel * 2 + 1][startIndex], noOfSamples * sizeof(int16_t));
					}

				}
			}
		}
	}
}

/****************************************************************************
* SetDefaults - set up channel voltage scales, coupling and offset
****************************************************************************/
void SetDefaults(UNIT *unit)
{

	for (int32_t ch = 0; ch < ChannelCount; ch++)
	{
		status = ps4000aSetChannel(unit->handle,										//handle to select the correct device
			(PS4000A_CHANNEL)(PS4000A_CHANNEL_A + ch),									//channel
			unit->channelSettings[PS4000A_CHANNEL_A + ch].enabled,						//if channel is enabled or not
			(PS4000A_COUPLING)unit->channelSettings[PS4000A_CHANNEL_A + ch].DCcoupled,	//if ac or dc coupling
			(PS4000A_RANGE)unit->channelSettings[PS4000A_CHANNEL_A + ch].range,			//the voltage scale of the channel
			unit->channelSettings[PS4000A_CHANNEL_A + ch].analogueOffset);				//analogue offset

		printf(status ? "SetDefaults:ps5000aSetChannel------ 0x%08lx for channel %i\n" : "", status, ch);
	}
}

/****************************************************************************
* OpenDevice
* Parameters
* - unit        pointer to the UNIT structure, where the handle will be stored
*
* Returns
* - PICO_STATUS to indicate success, or if an error occurred
***************************************************************************/
PICO_STATUS OpenDevice(UNIT *unit)
{
	int8_t line[100];
	int16_t r;
	status = ps4000aOpenUnit(&unit->handle, nullptr);// can call this function mutliple times to open multiple devices, also the unit.handle is sepcific to each device make sure you don't over write them
	if (unit->handle == 0)
	{
		return status;
	}

	//if not using 3.0 with device system needs to realise and set up accordingly
	if (status == PICO_USB3_0_DEVICE_NON_USB3_0_PORT)
	{
		status = ps4000aChangePowerSource(unit->handle, PICO_USB3_0_DEVICE_NON_USB3_0_PORT);

		if (status != PICO_OK)
		{
			return status;
		}
	}

	//device only has these min and max values
	unit->firstRange = PS4000A_10MV;
	unit->lastRange = PS4000A_50V;

	int8_t description[11][25] = { "Driver Version",
		"USB Version",
		"Hardware Version",
		"Variant Info",
		"Serial",
		"Cal Date",
		"Kernel Version",
		"Digital HW Version",
		"Analogue HW Version",
		"Firmware 1",
		"Firmware 2" };


	for (int i = 0; i <= 6; i++)
	{
		status = ps4000aGetUnitInfo(unit->handle, line, 100, &r, i);
		printf("%s:%s\n", description[i], line);
	}

	for (int ch = 0; ch < ChannelCount; ch++)
	{
		unit->channelSettings[ch].enabled = (ch == 0);
		unit->channelSettings[ch].DCcoupled = TRUE;
		unit->channelSettings[ch].range = PS4000A_5V;
		unit->channelSettings[ch].analogueOffset = 0.0f;
	}

	ps4000aMaximumValue(unit->handle, &unit->maxADCValue);
	SetDefaults(unit);
	return status;
}

/****************************************************************************
* Select input voltage ranges for channels
****************************************************************************/
void SetVoltages(UNIT *unit)
{
	int32_t i, ch, count = 0;
	for (i = unit->firstRange; i <= unit->lastRange; i++)
	{
		printf("%d -> %d mV\n", i, inputRanges[i]);
	}

	do
	{
		/* Ask the user to select a range */
		printf("Specify voltage range (%d..%d)\n", unit->firstRange, unit->lastRange);
		printf("99 - switches channel off\n");
		for (ch = 0; ch < ChannelCount; ch++)
		{
			printf("\n");
			do
			{
				printf("Channel %c: ", 'A' + ch);
				fflush(stdin);
				scanf_s("%hd", &(unit->channelSettings[ch].range));
			} while (unit->channelSettings[ch].range != 99 && (unit->channelSettings[ch].range < unit->firstRange || unit->channelSettings[ch].range > unit->lastRange));

			if (unit->channelSettings[ch].range != 99)
			{
				printf(" - %d mV\n", inputRanges[unit->channelSettings[ch].range]);
				unit->channelSettings[ch].enabled = TRUE;

				count++;
			}
			else
			{
				printf("Channel Switched off\n");
				unit->channelSettings[ch].enabled = FALSE;
				unit->channelSettings[ch].range = PS4000A_10MV;
			}
		}
		printf(count == 0 ? "\n** At least 1 channel must be enabled **\n\n" : "");
	} while (count == 0);	// must have at least one channel enabled

	SetDefaults(unit);	// Put these changes into effect
}

/****************************************************************************
* Stream Data Handler
* - Used by the two stream data examples - untriggered and triggered
* Inputs:
* - unit - the unit to sample on
***************************************************************************/
void StreamDataHandler(UNIT * unit)
{
	FILE * fp = NULL;
	BUFFER_INFO bufferInfo;
	int32_t i, j;
	PICO_STATUS status;
	uint32_t sampleInterval;
	int32_t index = 0;
	int32_t totalSamples = 0;
	uint32_t postTrigger;
	int16_t autostop;
	uint32_t downsampleRatio;
	uint32_t triggeredAt = 0;
	PS4000A_TIME_UNITS timeUnits;
	PS4000A_RATIO_MODE ratioMode;
	uint32_t preTrigger;
	int16_t powerChange = 0;

	for (i = 0; i < ChannelCount; i++)
	{

		if (unit->channelSettings[PS4000A_CHANNEL_A + i].enabled)
		{

			buffers[i * 2] = (int16_t*)calloc(bufferLength, sizeof(int16_t));
			buffers[i * 2 + 1] = (int16_t*)calloc(bufferLength, sizeof(int16_t));
			status = ps4000aSetDataBuffers(unit->handle, (PS4000A_CHANNEL)i, buffers[i * 2], buffers[i * 2 + 1], bufferLength, 0, PS4000A_RATIO_MODE_NONE);

			appBuffers[i * 2] = (int16_t*)calloc(bufferLength, sizeof(int16_t));
			appBuffers[i * 2 + 1] = (int16_t*)calloc(bufferLength, sizeof(int16_t));

			printf(status ? "StreamDataHandler:ps5000aSetDataBuffers(channel %ld) ------ 0x%08lx \n" : "", i, status);
		}
	}

	downsampleRatio = 1;
	timeUnits = PS4000A_US;
	sampleInterval = 1;
	ratioMode = PS4000A_RATIO_MODE_NONE;
	preTrigger = 0;
	//postTrigger = 0;
	postTrigger = 1000000;
	autostop = TRUE;

	bufferInfo.unit = unit;
	bufferInfo.driverBuffers = buffers;
	bufferInfo.appBuffers = appBuffers;

	if (autostop)
	{
		printf("\nStreaming Data for %lu samples", postTrigger / downsampleRatio);
		if (preTrigger)							// we pass 0 for preTrigger if we're not setting up a trigger
		{
			printf(" after the trigger occurs\nNote: %lu Pre Trigger samples before Trigger arms\n\n", preTrigger / downsampleRatio);
		}
		else
		{
			printf("\n\n");
		}
	}
	else
	{
		printf("\nStreaming Data continually\n\n");
	}

	g_autoStop = FALSE;

	printf("Collect streaming...\n");
	printf("Data is written to disk file (stream.txt)\n");
	printf("Press a key to start\n");
	_getch();

	do
	{
		//for streaming we use sample interval rather than timebase used in runblock
		if ((status = ps4000aRunStreaming(unit->handle, &sampleInterval, timeUnits, preTrigger, postTrigger, autostop, downsampleRatio, ratioMode,
			bufferLength)) != PICO_OK)
		{
			if (status == PICO_POWER_SUPPLY_CONNECTED || status == PICO_POWER_SUPPLY_NOT_CONNECTED || status == PICO_POWER_SUPPLY_UNDERVOLTAGE)
			{
				status = ps4000aChangePowerSource(unit->handle, status);
			}
			else
			{
				printf("StreamDataHandler:ps5000aRunStreaming ------ 0x%08lx \n", status);
				return;
			}
		}
	} while (status != PICO_OK);

	printf("Streaming data...Press a key to stop\n");
	fopen_s(&fp, "stream.txt", "w");

	if (fp != NULL)
	{
		fprintf(fp, "For each of the %d Channels, results shown are....\n", ChannelCount);
		fprintf(fp, "Maximum Aggregated value ADC Count & mV, Minimum Aggregated value ADC Count & mV\n\n");

		for (i = 0; i < ChannelCount; i++)
		{
			if (unit->channelSettings[i].enabled)
			{
				fprintf(fp, "   Max ADC    Max mV  Min ADC  Min mV   ");
			}
		}
		fprintf(fp, "\n");
	}

	while (!_kbhit() && !g_autoStop)
	{
		/* Poll until data is received. Until then, GetStreamingLatestValues wont call the callback */
		Sleep(0);
		g_ready = FALSE;

		status = ps4000aGetStreamingLatestValues(unit->handle, CallBackStreaming, &bufferInfo);

		index++;

		if (g_ready && g_sampleCount > 0) /* can be ready and have no data, if autoStop has fired */
		{
			if (g_trig)
			{
				triggeredAt = totalSamples + g_trigAt;		// calculate where the trigger occurred in the total samples collected
			}

			totalSamples += g_sampleCount;
			printf("\nCollected %3li samples, index = %5lu, Total: %7d samples", g_sampleCount, g_startIndex, totalSamples);

			if (g_trig)
			{
				printf("Trig. at index %lu total %lu", g_trigAt, triggeredAt);	// show where trigger occurred

			}

			for (i = 0; i < (uint32_t)(g_sampleCount); i++)
			{

				if (fp != NULL)
				{
					for (j = 0; j < ChannelCount; j++)
					{
						if (unit->channelSettings[j].enabled)
						{
							fprintf(fp,
								"Ch%C  %7d = %7dmV, %7d = %7dmV   ",
								(char)('A' + j),
								appBuffers[j * 2][i],
								adc_to_mv(appBuffers[j * 2][i], unit->channelSettings[PS4000A_CHANNEL_A + j].range, unit),
								appBuffers[j * 2 + 1][i],
								adc_to_mv(appBuffers[j * 2 + 1][i], unit->channelSettings[PS4000A_CHANNEL_A + j].range, unit));
						}
					}

					fprintf(fp, "\n");
				}
				else
				{
					printf("\nCannot open the file %s for writing.\n", "stream.txt");
					return;
				}

			}
		}
	}

	ps4000aStop(unit->handle);

	if (fp != NULL)
	{

		fclose(fp);
	}

	if (!g_autoStop)
	{
		printf("\nData collection aborted\n");
		_getch();
	}
	else
	{
		printf("\nData collection complete.\n\n");
	}

	for (i = 0; i < ChannelCount; i++)
	{
		if (unit->channelSettings[i].enabled)
		{
			free(buffers[i * 2]);
			free(appBuffers[i * 2]);

			free(buffers[i * 2 + 1]);
			free(appBuffers[i * 2 + 1]);

			if ((status = ps4000aSetDataBuffers(unit->handle, (PS4000A_CHANNEL)i, NULL, NULL, 0, 0, PS4000A_RATIO_MODE_NONE)) != PICO_OK)
			{
				printf("ClearDataBuffers:ps5000aSetDataBuffers(channel %d) ------ 0x%08lx \n", i, status);
			}
		}
	}
}

/****************************************************************************
* CollectStreamingImmediate
*  this function demonstrates how to collect a stream of data
*  from the unit (start collecting immediately)
***************************************************************************/
void CollectStreamingImmediate(UNIT * unit)
{
	//disable trigger, no need to call three functions to trun off trigger
	if (status = ps4000aSetSimpleTrigger(unit->handle, 0, PS4000A_CHANNEL_A, 0, PS4000A_RISING, 0, 0) != PICO_OK)
	{
		printf("Error seeting trigger, Error Code: 0x%08lx\n", status);
		return;
	}

	StreamDataHandler(unit);
	return;
}

/****************************************************************************
* CollectStreamingImmediate
*  this function demonstrates how to collect a stream of data
*  from the unit (start collecting immediately)
***************************************************************************/
void CollectStreamingTriggered(UNIT * unit)
{
	PS4000A_CONDITION *_condition = new PS4000A_CONDITION;
	PS4000A_DIRECTION *direction = new PS4000A_DIRECTION;
	PS4000A_TRIGGER_CHANNEL_PROPERTIES *properties = new PS4000A_TRIGGER_CHANNEL_PROPERTIES;

	PS4000A_CHANNEL ch = PS4000A_CHANNEL_A;
	PS4000A_THRESHOLD_DIRECTION _direction = PS4000A_RISING;
	int16_t threshold = mv_to_adc(1000, unit->channelSettings[PS4000A_CHANNEL_A].range, unit); 

	//if this array contation more than one channel for example was initlised as _condition[2] the channels could be AND please remebert to increase nConditions accordingly
	//Channels are DONT_CARE if not stated
	_condition->condition = PS4000A_CONDITION_TRUE;
	_condition->source = ch;

	direction->channel = ch;
	direction->direction = _direction;

	properties->channel = ch;
	properties->thresholdLower = threshold;
	properties->thresholdLowerHysteresis = 0;
	properties->thresholdMode = PS4000A_LEVEL;
	properties->thresholdUpper = threshold;
	properties->thresholdUpperHysteresis = 0;


	//calling PS4000A_CLEAR|PS4000A_ADD removes all previous conditions and sets this new one
	//If you wish to OR channels this functions will have to be called again but with only PS4000A_ADD
	if (status = ps4000aSetTriggerChannelConditions(unit->handle, _condition, 1, (PS4000A_CONDITIONS_INFO)(PS4000A_CLEAR | PS4000A_ADD)) != PICO_OK)
	{
		printf("Error setting Trigger Channel Conditions, Error Code: 0x%08lx\n", status);
		return;
	}

	//only ever need to call this once all channel can be set up if we make the directions into an array to hold all channels
	if (status = ps4000aSetTriggerChannelDirections(unit->handle, direction, 1) != PICO_OK)
	{
		printf("Error setting Trigger Channel Directions, Error Code: 0x%08lx\n", status);
		return;
	}

	//only ever need to call this once all channel can be set up if we make the properties into an array
	if (status = ps4000aSetTriggerChannelProperties(unit->handle, properties, 1, 0, 0) != PICO_OK)
	{
		printf("Error setting Trigger Channel Properties, Error Code: 0x%08lx\n", status);
		return;
	}

	StreamDataHandler(unit);
}

int main(void)
{


	int8_t ch = '.';
	UNIT unit;


	status = OpenDevice(&unit);
	if (status != PICO_OK)//if unit not found or open no need to continue
	{
		printf("Picoscope devices failed to open or select power source\n error code: 0x%08lx\n", status);
		_getch();
		return 0;
	}


	while (ch != 'X')
	{
		printf("\n\n");
		printf("S - Immediate streaming                       V - Set voltages\n");
		printf("T - Triggered streaming\n");
		printf("                                              X - Exit\n");
		printf("Operation:");

		ch = toupper(_getch());

		printf("\n\n");
		switch (ch)
		{
		case 'S':
			CollectStreamingImmediate(&unit);
			break;

		case 'T':
			CollectStreamingTriggered(&unit);
			break;

		case 'V':
			SetVoltages(&unit);
			break;

		case 'X':
			break;

		default:
			printf("Invalid operation\n");
			break;
		}
	}
	ps4000aCloseUnit(unit.handle);
	return 1;
}
