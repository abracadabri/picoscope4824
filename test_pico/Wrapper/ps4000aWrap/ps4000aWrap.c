/**************************************************************************
*
* Filename:    ps4000aWrap.c
*
* Copyright:   Pico Technology Limited 2014
*
* Author:      HSM
*
* Description:
*   The source code in this release is for use with Pico products when 
*	interfaced with Microsoft Excel VBA, National Instruments LabVIEW and 
*	MathWorks MATLAB or any third-party programming language or application 
*	that is unable to support C-style callback functions or structures.
*
*	You may modify, copy and distribute the source code for the purpose of 
*	developing programs to collect data using Pico products to add new 
*	functionality. If you modify the standard functions provided, we cannot 
*	guarantee that they will work with the above-listed programming 
*	languages or third-party products.
*
*   Please refer to the PicoScope 4000 Series (A API) Programmer's Guide
*   for descriptions of the underlying functions where stated.
*
* History:
*
*	Feb14: Initial release.
*
***************************************************************************/

#include <windows.h>
#include <stdio.h>
#include "ps4000aWrap.h"

int16_t		_ready;
int16_t		_autoStop;
uint32_t	_numSamples;
uint32_t	_triggeredAt = 0;
int16_t		_triggered = FALSE;
uint32_t	_startIndex;
int16_t		_overflow = 0;

int16_t		_channelCount = 0; // Should be set to 8 from the main application for the PicoScope 4824
int16_t		_enabledChannels[PS4000A_MAX_CHANNELS] = {0, 0, 0, 0, 0, 0, 0, 0}; // Keep a record of the channels that are enabled

typedef struct tWrapBufferInfo
{
	int16_t *driverBuffers[PS4000A_MAX_CHANNEL_BUFFERS];
	int16_t *appBuffers[PS4000A_MAX_CHANNEL_BUFFERS];
	int32_t bufferLengths[PS4000A_MAX_CHANNELS]; // In order of A max, A min, B max, ... G min.

} WRAP_BUFFER_INFO;

WRAP_BUFFER_INFO _wrapBufferInfo;

/////////////////////////////////
//
//	Function declarations
//
/////////////////////////////////

/****************************************************************************
* Streaming Callback
*
* See ps4000aStreamingReady (callback)
*
****************************************************************************/
void __stdcall StreamingCallback(
    int16_t handle,
	int32_t noOfSamples,
	uint32_t startIndex,
	int16_t overflow,
	uint32_t triggerAt,
	int16_t triggered,
	int16_t autoStop,
	void * pParameter)
{
	int16_t channel = 0;
	WRAP_BUFFER_INFO * _wrapBufferInfo = NULL;
	
	if (pParameter != NULL)
	{
		_wrapBufferInfo = (WRAP_BUFFER_INFO *) pParameter;
	}

	_numSamples = noOfSamples;
	_autoStop = autoStop;
	_startIndex = startIndex;

	_triggered = triggered;
	_triggeredAt = triggerAt;

	_overflow = overflow;

	if (_wrapBufferInfo != NULL && noOfSamples)
	{
		for (channel = (int16_t) PS4000A_CHANNEL_A; channel < _channelCount; channel++)
		{
			if (_enabledChannels[channel])
			{
				if (_wrapBufferInfo->appBuffers && _wrapBufferInfo->driverBuffers)
				{
					// Max buffers
					if (_wrapBufferInfo->appBuffers[channel * 2]  && _wrapBufferInfo->driverBuffers[channel * 2])
					{
						memcpy_s (&_wrapBufferInfo->appBuffers[channel * 2][startIndex], noOfSamples * sizeof(int16_t),
							&_wrapBufferInfo->driverBuffers[channel * 2][startIndex], noOfSamples * sizeof(int16_t));
					}

					// Min buffers
					if (_wrapBufferInfo->appBuffers[channel * 2 + 1] && _wrapBufferInfo->driverBuffers[channel * 2 + 1])
					{
						memcpy_s (&_wrapBufferInfo->appBuffers[channel * 2 + 1][startIndex], noOfSamples * sizeof(int16_t),
							&_wrapBufferInfo->driverBuffers[channel * 2 + 1][startIndex], noOfSamples * sizeof(int16_t));
					}
				}
			}
		}
	}
  
  _ready = 1;
}

/****************************************************************************
* BlockCallback
*
* See ps4000aBlockReady (callback)
*
****************************************************************************/
void __stdcall BlockCallback(int16_t handle, PICO_STATUS status, void * pParameter)
{
  _ready = 1;
}

/****************************************************************************
* RunBlock
*
* This function starts collecting data in block mode without the requirement 
* for specifying callback functions. Use the IsReady function in conjunction 
* to poll the driver once this function has been called.
*
* Input Arguments:
*
* handle - the handle of the required device.
* preTriggerSamples  see noOfPreTriggerSamples in ps4000aRunBlock.
* postTriggerSamples  see noOfPreTriggerSamples in ps4000aRunBlock.
* timebase  see ps4000aRunBlock.
* segmentIndex  see ps4000aRunBlock.
*
*
* Returns:
*
* See ps4000aRunBlock return values.
*
****************************************************************************/
extern PICO_STATUS __declspec(dllexport) __stdcall RunBlock(int16_t handle, int32_t preTriggerSamples, int32_t postTriggerSamples, 
            uint32_t timebase, uint32_t segmentIndex)
{
	_ready = 0;
	_numSamples = preTriggerSamples + postTriggerSamples;

	return ps4000aRunBlock(handle, preTriggerSamples, postTriggerSamples, timebase, 
		NULL, segmentIndex, BlockCallback, NULL);
}

/****************************************************************************
* GetStreamingLatestValues
*
* facilitates communication with the driver to return the next block of 
* values to your application when capturing data in streaming mode. Use with 
* programming languages that do not support callback functions.
*
* Input Arguments:
*
* handle - the handle of the required device.
*
* Returns:
*
* See ps4000aGetStreamingLatestValues return values.
*
****************************************************************************/
extern PICO_STATUS __declspec(dllexport) __stdcall GetStreamingLatestValues(int16_t handle)
{
	_ready = 0;
	_numSamples = 0;
	_autoStop = 0;

	return ps4000aGetStreamingLatestValues(handle, StreamingCallback, &_wrapBufferInfo);
}

/****************************************************************************
* Available Data
*
* Returns the number of samples returned from the driver and shows 
* the start index of the data in the buffer when collecting data in 
* streaming mode.
*
* Input Arguments:
*
* handle - the handle of the required device.
* startIndex - on exit, an index to the first valid sample in the buffer 
*				(When data is available).
*
* Returns:
*
* 0 - Data is not yet available.
* Non-zero - the number of samples returned from the driver.
*
****************************************************************************/
extern uint32_t __declspec(dllexport) __stdcall AvailableData(int16_t handle, uint32_t *startIndex)
{
	if( _ready ) 
	{
		*startIndex = _startIndex;
		return _numSamples;
	}

	return 0;
}

/****************************************************************************
* AutoStopped
*
* Indicates if the device has stopped on collection of the number of samples 
* specified in the call to the ps4000aRunStreaming function (if the 
* ps4000aRunStreaming functions autostop flag is set).
*
* Input Arguments:
*
* handle - the handle of the required device.
*
* Returns:
*
* Non-zero - if streaming has autostopped.
*
****************************************************************************/
extern int16_t __declspec(dllexport) __stdcall AutoStopped(int16_t handle)
{
	if( _ready) 
	{
		return _autoStop;
	}
	else
	{
		return 0;
	}
}

/****************************************************************************
* IsReady
*
* This function is used to poll the driver to verify that data is ready to be 
* received. The RunBlock or GetStreamingLatestValues function must have been 
* called prior to calling this function.
*
* Input Arguments:
*
* handle - the handle of the required device.
*
* Returns:
*
* 0  Data is not yet available.
* Non-zero  Data is ready to be collected.
*
****************************************************************************/
extern int16_t __declspec(dllexport) __stdcall IsReady(int16_t handle)
{
	return _ready;
}

/****************************************************************************
* IsTriggerReady
*
* Indicates whether a trigger has occurred when collecting data in streaming 
* mode, and the location of the trigger point in the buffer.
*
* Input Arguments:
*
* handle - the handle of the required device.
* triggeredAt  on exit, the index of the sample in the buffer where the 
* trigger occurred.
*
* Returns:
*
* 0  The device has not triggered.
* Non-zero  The device has been triggered.
*
****************************************************************************/
extern int16_t __declspec(dllexport) __stdcall IsTriggerReady(int16_t handle, uint32_t *triggeredAt)
{
	if (_triggered)
	{
		*triggeredAt = _triggeredAt;
	}

	return _triggered;
}

/****************************************************************************
* ClearTriggerReady
*
* Clears the triggered and triggeredAt flags in relation to streaming mode 
* capture.
*
* Input Arguments:
*
* handle - the handle of the required device.
*
* Returns:
*
* 1
*
****************************************************************************/
extern int16_t __declspec(dllexport) __stdcall ClearTriggerReady(int16_t handle)
{
	_triggeredAt = 0;
	_triggered = FALSE;
	return 1;
}

/****************************************************************************
* setChannelCount
*
* Sets the number of channels on the device. This is used to assist with 
* copying data in the streaming callback.
*
* Input Arguments:
*
* handle - the device handle.
* channelCount - the number of channels on the device.
*
****************************************************************************/
extern void __declspec(dllexport) __stdcall setChannelCount(int16_t handle, int16_t channelCount)
{
	_channelCount = channelCount;
}

/****************************************************************************
* setEnabledChannels
*
* Sets the number of enabled channels on the device. This is used to assist with 
* copying data in the streaming callback.
*
* Input Arguments:
*
* handle - the device handle.
* enabledChannels - an array representing the channel states. This should be 
*					8 elements in size.
*
* Returns:
*
* 0 if successful,
* -1 if handle <= 0 or channelCount is out of range
****************************************************************************/
extern int16_t __declspec(dllexport) __stdcall setEnabledChannels(int16_t handle, int16_t * enabledChannels)
{

	if(handle > 0)
	{
		if(_channelCount > 0 && _channelCount <= PS4000A_MAX_CHANNELS)
		{
			memcpy_s((int16_t *)_enabledChannels, PS4000A_MAX_CHANNELS * sizeof(int16_t), 
				(int16_t *)enabledChannels, PS4000A_MAX_CHANNELS * sizeof(int16_t));
			return 0;
		}
	}

	return -1;

}

/****************************************************************************
* setAppAndDriverBuffers
*
* Set the application and corresponding driver buffer in order for the 
* streaming callback to copy the data for the channel from the driver buffer 
* to the application buffer.
*
* Input Arguments:
*
* handle - the device handle.
* channel - the channel number (should be a PS4000A_CHANNEL enumeration value).
* appBuffer - the application buffer.
* driverBuffer - the buffer set by the driver.
* bufferLength - the length of the buffers (the length of the buffers must be
*				 equal).
*
* Returns:
*
* 0, if successful
* -1, otherwise
****************************************************************************/
extern int16_t __declspec(dllexport) __stdcall setAppAndDriverBuffers(int16_t handle, int16_t channel, int16_t * appBuffer, int16_t * driverBuffer, int32_t bufferLength)
{
	if(handle > 0)
	{
		if(channel < PS4000A_CHANNEL_A || channel >= PS4000A_MAX_CHANNELS)
		{
			return -1;
		}
		else
		{
			_wrapBufferInfo.appBuffers[channel * 2] = appBuffer;
			_wrapBufferInfo.driverBuffers[channel * 2] = driverBuffer;
				
			_wrapBufferInfo.bufferLengths[channel] = bufferLength;

			return 0;
		}
	}
	else
	{
		return -1;
	}

}

/****************************************************************************
* setMaxMinAppAndDriverBuffers
*
* Set the application and corresponding driver buffers in order for the 
* streaming callback to copy the data for the channel from the driver max and 
* min buffers to the respective application buffers for aggregated data 
* collection.
*
* Input Arguments:
*
* handle - the device handle.
* channel - the channel number (should be a PS4000A_CHANNEL enumeration value).
* appMaxBuffer - the application max buffer.
* appMinBuffer - the application min buffer.
* driverMaxBuffer - the max buffer set by the driver.
* driverMinBuffer - the min buffer set by the driver.
* bufferLength - the length of the buffers (the length of the buffers must be
*				 equal).
*
* Returns:
*
* 0, if successful
* -1, otherwise
****************************************************************************/
extern int16_t __declspec(dllexport) __stdcall setMaxMinAppAndDriverBuffers(int16_t handle, int16_t channel, int16_t * appMaxBuffer, int16_t * appMinBuffer, int16_t * driverMaxBuffer, int16_t * driverMinBuffer, int32_t bufferLength)
{
	if(handle > 0)
	{
		if(channel < PS4000A_CHANNEL_A || channel >= PS4000A_MAX_CHANNELS)
		{
			return -1;
		}
		else
		{
			_wrapBufferInfo.appBuffers[channel * 2] = appMaxBuffer;
			_wrapBufferInfo.driverBuffers[channel * 2] = driverMaxBuffer;

			_wrapBufferInfo.appBuffers[channel * 2 + 1] = appMinBuffer;
			_wrapBufferInfo.driverBuffers[channel * 2 + 1] = driverMinBuffer;

			_wrapBufferInfo.bufferLengths[channel] = bufferLength;

			return 0;
		}
	}
	else
	{
		return -1;
	}
}