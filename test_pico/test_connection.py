# -*- coding: utf-8 -*-
"""
Created on Mon Nov 10 10:34:22 2014

@author: Helene
"""
import pylab as plt
import numpy as np
import ps4000a
import time



#from ctypes import byref, POINTER, create_string_buffer, c_float, \
#    c_int16, c_int32, c_uint32, c_void_p, CFUNCTYPE, WINFUNCTYPE
#from ctypes import *
    
#BUFFER_INFO = [("Units",(POINTER(c_int16))),
 #           ("Driver_buffer",POINTER(np.array)),
  #          ("App_buffer",POINTER(np.array))]
#LIBNAME="ps4000a"

ps = ps4000a.PS4000a(serialNumber=None, connect=True, debug=False)
info=ps.getAllUnitInfo()
print(info)


#ps4000aStreamingReady = CFUNCTYPE(c_void_p,c_uint32, c_uint32, c_int16, c_uint32, c_int16,
#                    c_int16, c_void_p)

#GetStreamingLatestValues= ps.lib.ps4000aGetStreamingLatestValues
#GetStreamingLatestValues.argtypes=[c_int16, ps4000aStreamingReady, c_void_p]
#GetStreamingLatestValues.restype= c_int16

#arg = c_float(42)

#def CallBackStreaming(noSamples,startIndex,overflow,triggerAt, triggered, autoStop,
#                      pParameter):        
#   print("victory!")     
#   print(pParameter)
#   print(triggered)
   #print(type(data))
   #print(data)
#   print(noSamples)      
#   print(startIndex) 
#   print(triggerAt)
#   print(overflow)
#   print(autoStop)           
#   g_sampleCount = noSamples
#   g_startIndex   = startIndex
#   app_buffer=data_buffer
   #print(pParameter)
#   dataV=ps.rawToV(0,data_buffer)
   #print(dataV)   
   #print(ps.dataPtr[0])
  # g_autoStopped      = autoStop
  # g_ready = True

#   plt.plot(dataV)
#   plt.show()
  # g_triggered = triggered

   #if triggered==True:
    #  g_trigAt = triggerAt



#cmp_func= ps4000aStreamingReady(CallBackStreaming)

waveform_desired_duration = 50E-3
obs_duration = 3 * waveform_desired_duration
sampling_interval = 1E-3

(actualSamplingInterval, nSamples, maxSamples) = \
ps.setSamplingInterval(sampling_interval, obs_duration)
print("Sampling interval = %f ns" % (actualSamplingInterval * 1E9))
print("Taking  samples = %d" % nSamples)
print("Maximum samples = %d" % maxSamples)

ps.noSamples=1000
channelRange= ps.setChannel(channel='A', coupling="AC", VRange=2.0, VOffset=0.0,
           enabled=True, BWLimited=False, probeAttenuation=1.0)
channelRange= ps.setChannel(channel='B', coupling="AC", VRange=2.0, VOffset=0.0,
           enabled=True, BWLimited=False, probeAttenuation=1.0)
              
print("Chosen channel range = %d" % channelRange)
ps.flashLed(5)
ps.setSimpleTrigger('A', 1.0, 'Falling', delay=0, timeout_ms=100, enabled=True)

#pretrig=0
#downSampleRatio=1
downSampleMode=0
segmentIndex=0
data_buffer=(np.empty(ps.noSamples, dtype=np.int16))
#app_buffer=(np.empty(ps.noSamples, dtype=np.int16))

#sprint(data_buffer)
ps._lowLevelSetDataBuffer(0, data_buffer, downSampleMode, segmentIndex)
ps._lowLevelSetDataBuffer(1, data_buffer, downSampleMode, segmentIndex)
            #data_buffer_after = np.empty(numSamples, dtype=np.int16)
            #data= [] #np.empty(dtype=np.int16)

#data_buffer_app=(np.empty(nSamples, dtype=np.int16))
#sampleInterval=int(1E-6*1000000)
#print(sampleInterval)
#print(ps.noSamples)
#print(len(data_buffer))
#ps._lowLevelRunStreaming(sampleInterval, 3, int(ps.noSamples * pretrig),
 #                             int(ps.noSamples * (1 - pretrig)), downSampleRatio,
  #                         downSampleMode, len(data_buffer))
        

#print(data_buffer)                                    
#for i in range(30) : 
 #   print(i)
  #  time.sleep(0.1)
   # ps._lowLevelGetStreamingLatestValues(cmp_func, arg)
   # ps.lib.ps4000aGetStreamingLatestValues(c_int16(ps.handle),
    #                                             cmp_func, byref(arg))
    #print(dataV)
    #print(len(dataV))
    #print(len(data_buffer))
    #ps._lowLevelGetValuesAsync(nSamples, 0, 1,
     #                      0, segmentIndex, cmp_func)
    #print(data_buffer)
    
    #print(data_buffer)
           # self._lowLevelStreamingReady(nSamples, startIndex=0, overflow=0, triggerAt=0,
            #                    triggered=0,autostop=0)
            #self._lowLevelNoOfStreamingValues()
            #print("Mais pas la")
            #print(test)            
            
            #print("the length of the data is:")
            #print(data_buffer)
            #self.emit(QtCore.SIGNAL("data(PyQt_PyObject)"), np.array(data_buffer))
#ps.stop()    
        #self._lowLevelGetValuesAsync(nSamples, 0, 2, 1,0)
    
#print("this is after the function lowLevelGetStreamingLatestValue") 
#for i in range(8):
 #   ps._lowLevelClearDataBuffer(i, 0)

#info=ps.getAllUnitInfo()

#ps.setSigGenBuiltInSimple(offsetVoltage=0, pkToPk=1, waveType="Sine", frequency=5, shots=0, triggerType="Falling", triggerSource="None")
#ps.setSigGenBuiltInSimple()
#ps.close()

ps.runBlock()

print(ps.isReady())
   
ps.waitReady()
print(ps.isReady())
print("Waiting for awg to settle.")
time.sleep(2.0)
ps.runBlock()
ps.waitReady()
print("Done waiting for trigger")
dataA = ps.getDataV('A', nSamples, returnOverflow=False)
print(dataA)
dataTimeAxis = np.arange(nSamples) * actualSamplingInterval

ps.stop()
ps.close()

    #Uncomment following for call to .show() to not block
    #plt.ion()
    
plt.figure()
plt.hold(True)
plt.plot(dataTimeAxis, dataA, label="Clock")
plt.grid(True, which='major')
plt.title("Picoscope 2000 waveforms")
plt.ylabel("Voltage (V)")
plt.xlabel("Time (ms)")
plt.legend()
plt.show()

ps.close()
