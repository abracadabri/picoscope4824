import os

def get_config_setting(setting,config_file_name="config.txt"):
    """
        gets a setting from the configuration file
    """
    try:
        config_file = open(config_file_name)
    except IOError as e:
        print "No configuration file "+config_file_name+"  found"
    
    file_name=None
    
    for line in config_file:
        [left,right] = line.split("=")
        left = left.strip()
        right = right.strip()
        if left == setting:
            file_name = right
    if not file_name:
        raise Exception("Configuration file does not contain "+setting+"= line.")
    
    config_file.close()

    return file_name

def get_settings_name():
    return get_config_setting("SETTINGS")

def get_script_name():
    return get_config_setting("SCRIPT")
   
def get_drivers_path():
    return get_config_setting("DRIVERS")
    
def get_channel_number():
    return int(get_config_setting("CHAN_NUM"))
    
def get_debug_setting():
    setting=get_config_setting("DEBUG")
    if setting=='False':
        debug = False
    else:
        debug = True
    return debug


def get_drivers(drivers_path):
    """
        Returns the drivers names, their parameters and the corresponding 
        units of all drivers modules contained in a specified folder.
        The driver module needs to contain a class "Instrument" and a 
        dictionary "param" containing the different parameters and their units.
    """
    instruments=[]
    params={}
    units={}
#    instruments.append('TIME')
    params['']=[]
#    params['TIME']=[]
    #list all the .py files in the drivers folder
    for file_name in os.listdir(drivers_path):
        if file_name.endswith(".py") and not file_name=='Tool.py':
            name=file_name.split('.py')[0]
#            print name
            #add to instruments list
            instruments.append(name)
            #import the module of the instrument
            driver=__import__(name)
            #create an array for the parameters
            params[name]=[]
            #load the parameters and their units from the module
            try:
                for chan,u in driver.param.items():
                    units[chan]=u
                    params[name].append(chan)
            except:
                print "Invalid driver file: " + file_name + " (no param variable found)"
    print  [instruments,params,units]
    return [instruments,params,units]
    
    
def get_voltage_range():
    return int(get_config_setting("VRANGE", "settings.txt"))

def get_sampling_interval():
    return int(get_config_setting("SAMPLING_INTERVAL","settings.txt"))
    
def get_obs_duration():
    return int(get_config_setting("OBS_DURATION", "settings.txt"))