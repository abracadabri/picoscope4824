# -*- coding: utf-8 -*-
"""
Created on Thu Nov 06 09:42:25 2014

@author: Helene
"""

import sys, collections
from PyQt4 import QtGui, QtCore
from numpy import mod
import matplotlib.pyplot as plt
#import plot_graphs

pos = collections.namedtuple("pos", ("row, column"))

class ChannelsHub(QtGui.QWidget):

    # Checks whether Picoscope is open
    
    # Read number of channels
   
   
    color_set = ['cyan', 'black', 'blue', 'red', 'green', 'orange', 'magenta', 'maroon', 'plum','violet'] 
    itemSelected = QtCore.pyqtSignal(QtGui.QWidget, pos)
    
    def __init__(self,parent=None, num_channels=8):
        
        super(ChannelsHub, self).__init__(parent)
        
        self.num_channels= num_channels
        #This is a grid strictured layout        
        self.grid = QtGui.QGridLayout()  
        self.grid.setSpacing(10)
        self.color_btn_list=[]
        #initilize the lists
        #self.set_lists(8)
        self.resize(480, 600)  
        #self.add_channel()
        #self.add_channel()
    
        self.headers=["Color","Channel Name","Active","Show on graph","Range [V]"]
        for i,hdr_text in enumerate(self.headers):
            header = QtGui.QLabel(hdr_text)
            font = QtGui.QFont()            
            font.setBold(True)
            header.setFont(font)
            self.grid.addWidget(header, 0, i)
            self.grid.setAlignment(header, QtCore.Qt.AlignHCenter)
        
        for i in range(self.num_channels):
            self.add_channel(i)
   
        self.setLayout(self.grid) 
        
        
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('QtGui.QCheckBox')
        self.show()
    
    def add_channel(self, idx_channel):
        color=None         
        self.create_color_btn(self.color_btn_list, 0, color)        
        self.cb_active = QtGui.QCheckBox('', self)
        self.cb_active.stateChanged.connect(self.cb_active_handler)
        cb2 = QtGui.QCheckBox('', self) 
        channel_name= QtGui.QLabel('Channel A', self)
        V_range = QtGui.QComboBox(self)
        V_range.addItem("0.2")
        V_range.addItem("0.5")
        #hspacelabel = QtGui.QLabel('hspace')
        
        self.grid.addWidget(channel_name, 1 + idx_channel, 1)
        self.grid.setAlignment(channel_name, QtCore.Qt.AlignHCenter)
        self.grid.addWidget(self.cb_active, 1 + idx_channel, 2)
        self.grid.setAlignment(self.cb_active, QtCore.Qt.AlignHCenter)
        self.grid.addWidget(cb2, 1 + idx_channel, 3)
        self.grid.setAlignment(cb2, QtCore.Qt.AlignHCenter)
        self.grid.addWidget(V_range,1 + idx_channel ,4)
        self.grid.setAlignment(V_range, QtCore.Qt.AlignHCenter)
      
        #self.setLayout(vbox)    
    def create_color_btn(self, color_btn_list, col, color):

        index=len(color_btn_list)     
                
        if not color:
            color = self.color_set[mod(index,10)]
            
        btn=QtGui.QPushButton(self)
       # btn.setObjectName(_fromUtf8("param_name_le_list "+str(index)))
        btn.setStyleSheet('QPushButton {background-color: %s}'%color)
        btn.setFixedSize(20,20)
        
        self.connect(btn, QtCore.SIGNAL("clicked()"),self.color_btn_handler) 
        #add the newly created widget to the list and tothe layout
        color_btn_list.append(btn) 
        self.grid.addWidget(btn, index+1, col,1,1)  
        
               
    def color_btn_handler(self):
        btn = self.sender()
        idx=self.color_btn_list.index(btn)

        color = QtGui.QColorDialog.getColor(initial = btn.palette().color(1))
        btn.setStyleSheet('QPushButton {background-color: %s}'%color.name())
        print self.get_color_list()
        self.emit(QtCore.SIGNAL("colorsChanged()")) 
        
    def cb_active_handler(self, state):
        button = self.sender()
        index = self.grid.indexOf(button)
        row, column, cols, rows = self.grid.getItemPosition(index)
        #print button 
        #print index, row, column        
        
        if state == QtCore.Qt.Checked:

            self.emit(QtCore.SIGNAL("Channel_is_active"),True, row)  
            #self.emit(QtCore.SIGNAL("row_index(int)"), row)
            #plot_graphs.MyWindow.plot_data_array([1,2,3,4])
        else:
            self.emit(QtCore.SIGNAL("Channel_is_active"),False, row) 
            #self.emit(QtCore.SIGNAL("row_index(int)"), row)
            #print "False..."
                
        
def main():
    
    app = QtGui.QApplication(sys.argv)
    ex = ChannelsHub()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
        
        
        
  