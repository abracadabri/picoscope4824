# -*- coding: utf-8 -*-
"""
Created on Thu Nov 13 15:59:25 2014

@author: Helene
"""


import sys
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QReadWriteLock
import QtTools
import PicoWindows as plt
import pylab
import DataManagement as DM
import numpy as np
import ps4000a
import time
import os
#import pycoscope
from readconfigfile import get_channel_number,get_debug_setting
from readsettingfile import get_voltage_range,get_obs_duration, get_sampling_interval

class MainWindow(QtGui.QMainWindow):
    
    def __init__(self):
        super(MainWindow, self).__init__()


        #charge les parametres generaux a partir du fichier config.txt
        self.channel_number=get_channel_number()
        
        self.debug=get_debug_setting()     
        
        self.v_range=get_voltage_range()
        
        self.sampling_interval= get_sampling_interval()
        
        self.obs_duration= get_obs_duration()
        
        self.integrated_sig=np.array([])
        print "The number of channels is %i"%(self.channel_number)
    
        if self.debug:
            print "="*20
            print "Debug mode on"
            print "="*20
            
        
        self.initUI()
        self.initPI()

    def initPI(self):
        """
        Here we define all the attributes needed to collect data with the scope
        """
        self.connect_instr()
        self.idx=0
        self.n_active_channels=8
        if self.debug==False:
            self.acq_duration=1
            self.laser_rep_period= 0.0005
            self.set_channels_properties()
            self.lock = QReadWriteLock()
            self.data_array=np.array([])
            self.datataker = DM.DataTaker(self.lock,self.scope, self.n_active_channels,
                                       self)
            self.set_trigger_conditions()
            self.set_acquisition_parameters(self.obs_duration,self.sampling_interval)
        else:
            self.scope=None
            self.lock = QReadWriteLock()
            self.datataker = DM.DataTaker(self.lock,self.scope, self.n_active_channels,
                                       self)
            self.actualSamplingInterval=1
            self.n_active_channels=8
     
		#create a lock

        
        self.connect(self.datataker, QtCore.SIGNAL("data(PyQt_PyObject)"),self.update_data)
        self.connect(self.datataker, QtCore.SIGNAL("script_finished(bool)"),self.finished_DTT)
    #self.data_taker= data_taker. (start, stop, read_data)
#    self.connect(self.datataker, SIGNAL("data(PyQt_PyObject)"),self.update_data)


        
    def initUI(self):
    
#To use Latex fonts
#import pylab to copy at beginning of file
#        fontProperties = {'family':'serif','serif':['normal'],'weight' : 'normal', 'size' : 15}
#        pylab.rc('text', usetex=True)
#        pylab.rc('font', **fontProperties)
# Then write r"$E_x$" instead of "Ex" to have it in LaTex
        
        ##### start/stop/pause buttons #####
        """
        this comes from QtTools.py
        it makes buttons you can click and it calls the function in the slot argument
        the image should be in the folder images and be a png file
        """
        
        self.enter_duration= QtGui.QDoubleSpinBox(self)
        self.enter_duration.setMinimum(0)
        self.enter_duration.setMaximum(20)
        self.enter_duration.setValue(1)
      
        self.enter_no_shots= QtGui.QSpinBox(self)
        self.enter_no_shots.setMinimum(1)
        self.enter_no_shots.setMaximum(120)
        self.enter_no_shots.setValue(120)
        
        self.start_DTT_action=QtTools.create_action(self,"Start DTT",slot=self.start_DTT,shortcut=QtGui.QKeySequence("Enter"),icon="start",tip="Launch Data Taker")        
        self.stop_DTT_action=QtTools.create_action(self,"Stop DTT",slot=self.stop_DTT,shortcut=QtGui.QKeySequence("F6"),icon="stop",tip="Stop Data Taker")
        self.stop_DTT_action.setEnabled(False)        
       
        self.instToolbar = self.addToolBar("Instruments")
        self.instToolbar.setObjectName ( "InstToolBar")
        
        self.instToolbar.addWidget(QtGui.QLabel("Acquire during [s] :"))
        self.instToolbar.addWidget(self.enter_duration)
        self.instToolbar.addAction(self.start_DTT_action)
        self.instToolbar.addAction(self.stop_DTT_action) 
        self.instToolbar.addWidget(self.enter_no_shots)
        
                
        """
        tu veux que le parent de ton objet MyWindow soit l'objet MainWindow
        """
        self.plt_raw= plt.FancyScopeScreen(self,channel_number=8,ofname="C:\Users\Helene\Documents\Work\McGill\PhD\Code\PicoScope4824_BitBucket\streamdata.dat",track_signal=True)
       # self.connect(self.channels_info,QtCore.SIGNAL("row_index(int))"),self.get_row_index)  
        
        
        self.plt_raw.update_labels(["TRIGGER","","R","L","+45","-45","90","0"])   
        
        plt_DockWidget = QtGui.QDockWidget("Raw Signals", self)
        plt_DockWidget.setObjectName("plt_DockWidget")
        plt_DockWidget.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea|QtCore.Qt.RightDockWidgetArea)
        #self.connect(self.startWidget.startStopButton,SIGNAL('clicked()'),self.launch_DTT)
        plt_DockWidget.setWidget(self.plt_raw)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, plt_DockWidget)
#        self.plt_raw.add_channel_controls()
        self.connect(self, QtCore.SIGNAL("data_array_updated(PyQt_PyObject)"),
                     self.plt_raw.update_plot)
        self.connect(self.plt_raw,QtCore.SIGNAL("limits_changed(int,PyQt_PyObject)"),self.emit_axis_lim)
        QtGui.QToolTip.setFont(QtGui.QFont('SansSerif', 14))
        
        self.plt_raw.change_x_axis_label('Time [ms]')
        self.plt_raw.change_y_axis_label('Voltage [V]') 
       
        self.plt_components=plt.FancyScopeScreen(self,channel_number=4)
#        self.plt_components= plt.FancyScopeScreen(self,channel_number=4, labels=["Sx + Sy", "Sx - Sy", "S+ - S-", "Sr - Sl"])
        pltC_DockWidget = QtGui.QDockWidget("Component Signals", self)
        pltC_DockWidget.setObjectName("pltC_DockWidget")
        pltC_DockWidget.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea|QtCore.Qt.RightDockWidgetArea)
        #self.connect(self.startWidget.startStopButton,SIGNAL('clicked()'),self.launch_DTT)
        pltC_DockWidget.setWidget(self.plt_components)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, pltC_DockWidget) 
        
        self.plt_components.update_labels(["(Sx + Sy)/(Sx + Sy)", "(Sx - Sy)/(Sx + Sy)", "(Sp - Sm)/(Sp + Sm)", "(Sr - Sl)/(Sr + Sl)"])   

        self.plt_components.change_x_axis_label('Time [s]')
        self.plt_components.change_y_axis_label('Voltage [V]') 
 
 
        self.tabifyDockWidget(plt_DockWidget, pltC_DockWidget)
        
        self.plt_degree_pol= plt.ScopeScreen(self,channel_number=1, labels=["Degree of Polarization"])
        pltD_DockWidget = QtGui.QDockWidget("Degree of polarization", self)
        pltD_DockWidget.setObjectName("pltD_DockWidget")
        pltD_DockWidget.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea|QtCore.Qt.RightDockWidgetArea)
        #self.connect(self.startWidget.startStopButton,SIGNAL('clicked()'),self.launch_DTT)
        pltD_DockWidget.setWidget(self.plt_degree_pol)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, pltD_DockWidget) 
    
        self.connect(self,   QtCore.SIGNAL("data_components_updated(PyQt_PyObject)"),
                     self.plt_components.update_plot)
        self.plt_degree_pol.x_label('Time [s]')
        self.plt_degree_pol.y_label('Voltage [V]') 
      
        
        self.plt_Pointcarre= plt.ScopeScreen(self,channel_number=1, labels=["Ex and Ey"])
        pltP_DockWidget = QtGui.QDockWidget("Pointcarre representation", self)
        pltP_DockWidget.setObjectName("pltP_DockWidget")
        pltP_DockWidget.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea|QtCore.Qt.RightDockWidgetArea)
        #self.connect(self.startWidget.startStopButton,SIGNAL('clicked()'),self.launch_DTT)
        pltP_DockWidget.setWidget(self.plt_Pointcarre)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, pltP_DockWidget) 
        
        self.plt_Pointcarre.x_label('Ex ')
        self.plt_Pointcarre.y_label('Ey ') 
        pltP_DockWidget.setMaximumWidth(500)
        
        self.tabifyDockWidget(pltD_DockWidget, pltP_DockWidget)
        
        QtGui.QToolTip.setFont(QtGui.QFont('SansSerif', 14))
       

        exitAction = QtGui.QAction(QtGui.QIcon('exit.png'), '&Exit', self)        
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(QtGui.qApp.quit)
        
        saveAction = QtGui.QAction('Save', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.setStatusTip('Save current file')
        saveAction.triggered.connect(self.saveFile)


        self.statusBar()

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(saveAction)
        fileMenu.addAction(exitAction)  
     
        self.instToolbar2 = self.addToolBar("Save and Quit")
        self.instToolbar2.setObjectName ( "InstToolbar2")
        
        self.save_action=QtTools.create_action(self,"Save Data",slot=self.saveFile,shortcut=QtGui.QKeySequence("F8"),icon="save",tip="Save Data")                 
        self.instToolbar2.addAction(self.save_action)
        self.exit_action=QtTools.create_action(self,"Exit programme",slot=QtGui.qApp.quit,shortcut=QtGui.QKeySequence("F9"),icon="exit",tip="Exit Programme (F9)")                 
        self.instToolbar2.addAction(self.exit_action)
    
        
        self.resize(1000, 600)
        self.center()
        
        self.setWindowTitle('Ellipsometer - Picoscope 4824 controller')    
        self.show()
        
    def update_data(self,new_data_set):
  
        print "the data is updated"
        print len(new_data_set)
        
        
        #time_axis=self.actualSamplingInterval*1000*np.arange(len(new_data_set[0])) 
        time_axis=np.arange(len(new_data_set[0]))
        #print len(time_axis)        
        #print(self.actualSamplingInterval)
   
        for i in range(self.n_active_channels):  
            
            self.plt_raw.clear_plot(i)
            self.plt_raw.plot_data_array(i, time_axis, new_data_set[i])
            #self.plt_raw.clear_plot(i +2 )  
            #self.plt_raw.plot_data_array(i +2, time_axis[1:5000], new_data_set[i + 2][1:5000])
      
            #self.plt_raw.set_axis_scale(0, time_axis, -1, 5)  
            self.emit(QtCore.SIGNAL("data_array_updated(PyQt_PyObject)"), new_data_set)
        #trigger_idx=[200, 400]
        
        trigger_idx= self.get_triggered_indexes(new_data_set[1], new_data_set[0], 2)
        integrated_signals= self.integrate_signal(new_data_set[1:self.n_active_channels], trigger_idx)
        
        #self.plt_components.clear_plot() 
      
        #for j in range((self.n_active_channels -1)/2):   
        #self.plt_components.plot_data_array(integrated_signals[j]- integrated_signals[j+1])
        """
        #By definition: Channel B (idx 1 of new_data_set) : Sx
         #              Channel C (idx 2) : Sy
          #             Channel D (idx 3) : S+
           #            Channel E (idx 4) : S-
            #           Channel F (idx 5) : Sl
             #          Channel G (idx 6) : Sr
        """
        Sx= integrated_signals[5]
        Sy= integrated_signals[6]
        Sp= integrated_signals[3]
        Sm= integrated_signals[4]
        Sl= integrated_signals[2]
        Sr= integrated_signals[1]
        
        data_components=[]
        data_components.append(np.divide(Sx + Sy, Sx + Sy))
        data_components.append(np.divide(Sx - Sy, Sx + Sy))
        data_components.append(np.divide(Sp - Sm, Sp + Sm))
        data_components.append(np.divide(Sr - Sl, Sr + Sl))
        #data_components.append(Sx)
        #data_components.append(Sy)
        #data_components.append(Sp)
        #data_components.append(Sm)
   
        self.emit(QtCore.SIGNAL("data_components_updated(PyQt_PyObject)"), np.array(data_components))
        #time_axis_components=self.laser_rep_period*np.arange(len(integrated_signals[0]))    
        time_axis_components= np.arange(len(integrated_signals[0])) 
        """
        #This is Sx + Sy
        """
        self.plt_components.clear_plot(0)
        self.plt_components.plot_data_array(0, time_axis_components, np.divide((Sx + Sy), Sx+ Sy)) 
        
        print("this is the ratio that we want to be 1")
        print(Sx)
        print("this is Sy")
        print(Sy)
        """
        #This is Sx - Sy
        """
        self.plt_components.clear_plot(1)
        self.plt_components.plot_data_array(1,time_axis_components, np.divide((Sx - Sy),(Sx + Sy)))  
        """
        #This is S+ - S-
        """
        self.plt_components.clear_plot(2)
        self.plt_components.plot_data_array(2,time_axis_components, np.divide((Sp - Sm),(Sp + Sm))) 
        """
        #This is Sr - Sl
        """
        
        #self.plt_components.set_axis_scale(time_axis_components[0], time_axis_components[len(time_axis_components) -1] , -50,50)  
        self.plt_components.clear_plot(3)
        self.plt_components.plot_data_array(3,time_axis_components, np.divide((Sr - Sl),(Sr + Sl)))
        
        """
        #Degree of polarization: 
        """
        #S0= 1/(Sx + Sy)
        
        #Degree_Pol= np.divide((np.power((Sx- Sy),2) + np.power((Sp- Sm),2) + np.power((Sr- Sl),2) ), Sx + Sy)
        #weaker version of polarization        
        Degree_Pol= np.divide(np.sqrt(np.power(Sx- Sy,2)), Sx + Sy) #+ \
                    #np.divide(np.sqrt(2*np.power(Sr- Sl,2)), Sr + Sl)
        print("This is the degree of polarization")        
        print(Degree_Pol)
        self.plt_degree_pol.clear_plot(0)        
        self.plt_degree_pol.plot_data_array(0, time_axis_components, Degree_Pol)
        self.plt_degree_pol.set_axix_scale(time_axis_components[0], time_axis_components[len(time_axis_components) -1] , 0,1)  
        """
        #Works out Cartesian complex-plane representation 
        """
        
        Amp_Xhi= np.sqrt(np.divide(Sx,Sy))
        print("max Amp Xhi")
        print Amp_Xhi
        #Amp_Xhi=Amp_Xhi/np.max(Amp_Xhi)
        #cos_Xhi= ((Sp - Sm)/(Sp + Sm))*((Sx + Sy)/(2*np.sqrt(Sx*Sy)))
        normalisation=np.max((Sl -Sr)/(Sl + Sr))
        print("normalisation is:")
        print normalisation
        cos_Xhi= ((Sl - Sr)/(Sl + Sr))*((Sx + Sy)/(2*np.sqrt(Sx*Sy)))/normalisation

        Xhi= np.arccos(cos_Xhi)
        print max(Xhi)
        print min(Xhi)
        
        sin_Xhi=np.sin(Xhi)
        print min(sin_Xhi)
        print max(sin_Xhi)
        print max(cos_Xhi)
        print min(cos_Xhi)
        Ex= Amp_Xhi*cos_Xhi
        Ey= Amp_Xhi*sin_Xhi
        
        self.plt_Pointcarre.clear_plot(0)
        self.plt_Pointcarre.set_axix_scale(-1, 1 , -1,1)  
        self.plt_Pointcarre.draw_circle()
        self.plt_Pointcarre.turn_grid_on()
        self.plt_Pointcarre.draw_line_x(0)
        self.plt_Pointcarre.draw_line_y(0)
        #self.plt_Pointcarre.set_linestyle(0,'-')
        self.plt_Pointcarre.plot_data_array(0,Ex, Ey)
        
    def center(self):
        
        qr = self.frameGeometry()
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())
        
        
    def closeEvent(self, event):
        
        reply = QtGui.QMessageBox.question(self, 'Message',
            "Are you sure to quit?", QtGui.QMessageBox.Yes | 
            QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()    
            
    def saveFile(self):

        #self.setGeometry(QtCore.QRect(100, 100, 400, 200))
        #self.show()
        #filename = 
        self.plt_components.data_array
        
        Degree_pol=self.plt_degree_pol.get_y()
        Ex=self.plt_Pointcarre.get_x()
        Ey=self.plt_Pointcarre.get_y()
        
        filename=str(QtGui.QFileDialog.getSaveFileName(self, 'Save File'))
        #os.getenv('C:\Users\Helene\Documents\Work\McGill')
        exts=[".rawdat",".comp",".deg"]
        print filename
        base_filename=filename.split(".")[0]
        print base_filename
        for ext in exts:
            filename=base_filename+ext
            print filename
            
            of=open(filename,"w")
            if ext==".rawdat":
                of.write("# this file contains ... and ... the units are ...\n")
                of.close()
                of=open(filename,"a")
                np.savetxt(of,self.plt_raw.data_array)
            elif ext==".comp":
                of.write("# this file contains the components \n")
                of.close()
                of=open(filename,"a")
                np.savetxt(of,self.plt_components.data_array)
            elif ext==".deg":
                of.write("# this file contains the degree of polarization, the units are ...\n")
                of.close()
                of=open(filename,"a")
                np.savetxt(of,np.vstack([Ex,Ey]))
            of.close()
#        np.savetxt(filename, self.data_array)
        #f = open(filename, 'w')
        #filedata = self.text.toPlainText()
        #f.write(filedata)
        #f.close()
   
         
    def connect_instr(self):
   
        
        print("Attempting to open Picoscope 4824...")
        if self.debug:
            print "="*20
            print "Debug Mode"
            self.scope=None
            self.nSamples=100
        else:
            self.scope = ps4000a.PS4000a(debug=self.debug)
            self.scope.flashLed(5)
        print("Found the following picoscope:")
#        print(self.scope.getAllUnitInfo())
        
    def set_channels_properties(self):
        
         for i in range(self.channel_number):
             channelRange = self.scope.setChannel(i, 'DC', 5.0, 0.0, enabled=True, BWLimited=True)
             print("Chosen channel range = %d" % channelRange)
             
    def set_trigger_conditions(self):
        self.scope.setSimpleTrigger('A', 1, "Rising", delay=0, timeout_ms=5000, enabled=True)
        
        
    def set_acquisition_parameters(self, obs_duration, sampling_interval):
      
        if self.debug == False:
            (self.actualSamplingInterval, self.nSamples, self.maxSamples) = \
            self.scope.setSamplingInterval(sampling_interval, obs_duration)
            print("Initial Sampling interval = %f ns" % (sampling_interval* 1E9 ))           
            print("Sampling interval = %f ns" % (self.actualSamplingInterval * 1E9))
            print("Taking  samples = %d" % self.nSamples)
            print("Maximum samples = %d" % self.maxSamples)
        
        self.datataker.set_acquisition_parameters(obs_duration, self.actualSamplingInterval, self.nSamples)
    

    def get_triggered_indexes(self,trigger_signal_1, trigger_signal_2, trigger_cutoff):
        #get a numerotation of the indexes
        indexes=np.arange(len(trigger_signal_1))
        #select only the indexes where the trigger is higher than the cutoff
        interesting_indexes_1=indexes[trigger_signal_1>trigger_cutoff]
        interesting_indexes_2 = indexes[trigger_signal_2 > trigger_cutoff]
        #print("Those are the interesting indexes")        
        #print interesting_indexes_1
        #they will be a difference in the indexes between two trigger events but not within the same trigger event
        #we had a 2 at the end because this trigger event won't appear with this selection method.    
        variations_1=np.append(np.diff(indexes[trigger_signal_1>trigger_cutoff]),2)
        variations_2=np.append(np.diff(indexes[trigger_signal_2 > trigger_cutoff]),2)
        #print("Those are the variations")
        #print variations
        #print variations

        #with this we select the indexes at the end of each trigger event
        print("These are the interesting indexes 1")
        print interesting_indexes_1[variations_1>1]
        really_interesting_indexes_1= interesting_indexes_1[variations_1>1]
        print("These are the interesting indexes 2")
        print interesting_indexes_2[variations_2>1]
        really_interesting_indexes_2= interesting_indexes_2[variations_2>1]
        
        idx_to_remove=[]
        idx_to_keep=[]
        no_of_shots= self.enter_no_shots.value()
        for i in range(0,len(really_interesting_indexes_2)):
            idx = np.abs(really_interesting_indexes_1 - really_interesting_indexes_2[i]).argmin()
            idx_to_remove.append(idx -1)
            idx_to_remove.append(idx)
            idx_to_keep.append(really_interesting_indexes_1[idx + 1:idx + no_of_shots - 1])
        print("those are the indexes to remove")
        print idx_to_remove    
        #return interesting_indexes_1[variations_1>1] 
        print("This is the final things")
        print np.hstack(idx_to_keep)
        print("Those are the ones>")
        print(interesting_indexes_1)
        #print np.delete(really_interesting_indexes_1, idx_to_remove)
        #return np.delete(really_interesting_indexes_1, idx_to_remove)

        #return interesting_indexes_1
        return np.hstack(idx_to_keep)
        
    def integrate_signal(self,raw_signal, indexes):
        integrated_signal=[]
        for i in range(len(indexes)):
            try:
                #integrate the signal between the given indexes
                after_trigg= np.sum(raw_signal[:, indexes[i]: indexes[i] + 30],1)
                before_trigg=np.sum(raw_signal[:, indexes[i] -30: indexes[i]],1)
                diff= after_trigg - before_trigg
                integrated_signal.append(diff)
            except:
                #the last one is integrated until the end of the signal
                integrated_signal.append(np.sum(raw_signal[:,indexes[i]:],1))
            
        return np.transpose(np.vstack(integrated_signal))
        
    def start_DTT(self):
        
           
        self.obs_duration = self.enter_duration.value()
        print self.obs_duration
        self.set_acquisition_parameters(self.obs_duration,self.sampling_interval)
        
#        self.datataker.set_acquisition_parameters(obs_duration, sampling_interval)
       
       #if not isinstance(self.new_duration, int):
        #    self.pop_up= QtGui.QDialog()
         #   self.pop_up.exec_()
          #  print("You need to enter an [int]")
        #else:
         #   self.obs_duration=self.new_duration
        if self.datataker.isStopped():
            self.start_DTT_action.setEnabled(False)
            self.stop_DTT_action.setEnabled(True)
            

            #read the name of the script file to run
            """
            you can feed the script name to the datataker here if you might want to change it without 
            restarting the whole programm
            """
            #self.datataker.set_script(str(self.startWidget.scriptFileLineEdit.text()))   
        
            #this command is specific to Qthread, it will execute whatever is define in 
            #the method run() from DataManagement.py module
            self.datataker.start()
        else:         
            print "Couldn't start DTT - already running!"
            
    def stop_DTT(self):
        if not self.datataker.isStopped():  
            self.datataker.resume()
            self.datataker.stop()
#            self.output_file.close()  
            
            self.start_DTT_action.setEnabled(True)
            self.stop_DTT_action.setEnabled(False)
            
        else:         
            print "Couldn't stop DTT - it wasn't running!"
            
    def finished_DTT(self, completed):
        if completed:        
            self.start_DTT_action.setEnabled(True)
            self.stop_DTT_action.setEnabled(False)
            #just make sure the pause setting is left as false after ther run
            self.datataker.resume()
            
    def toggle_DTT(self):
        if self.datataker.isStopped():
            self.start_DTT()
        else:         
            self.stop_DTT()
    
    def emit_axis_lim(self,mode,limits):  
        print("Hello")
def main():
    
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    
    
    """remove that when the testing is over"""
    trig_off=np.ones(20)
    trig_on=np.ones(5)*5
    trig_signal=trig_off
    for i in range(3):
        trig_signal=np.append(trig_signal,trig_on)
        trig_signal=np.append(trig_signal,trig_off)
    chan_num=8
    raw_signal=np.transpose(np.vstack([np.ones(chan_num) for a in range(len(trig_signal))]))
    ex.plt_raw.update_plot(raw_signal)    
    ex.plt_components.update_plot(raw_signal)
    """remove that when the testing is over"""
    
    ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()