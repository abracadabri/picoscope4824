# -*- coding: utf-8 -*-
"""
Created on Tue Nov 18 22:50:23 2014

@author: pfduc
"""

import numpy as np
import time
#from random import random


def get_triggered_indexes(trigger_signal,trigger_cutoff):
    #get a numerotation of the indexes
    indexes=np.arange(len(trigger_signal))
    #select only the indexes where the trigger is higher than the cutoff
    interesting_indexes=indexes[trigger_signal>trigger_cutoff]
    print interesting_indexes
    #they will be a difference in the indexes between two trigger events but not within the same trigger event
    #we had a 2 at the end because this trigger event won't appear with this selection method.    
    variations=np.append(np.diff(indexes[trigger_signal>trigger_cutoff]),2)
    print("those are the variations")
    print variations
    
    #with this we select the indexes at the end of each trigger event
    print("These are the interesting indexes")
    print interesting_indexes[variations>1]
    return interesting_indexes[variations>1]
    
def integrate_signal(raw_signal, indexes):
    integrated_signal=[]
    for i in range(len(indexes)):
        try:
            #integrate the signal between the given indexes
            integrated_signal.append(np.sum(raw_signal[:,indexes[i]:indexes[i+1]],1))
        except:
            #the last one is integrated until the end of the signal
            integrated_signal.append(np.sum(raw_signal[:,indexes[i]:],1))
            
    return np.transpose(np.vstack(integrated_signal))
    
    
if __name__ == "__main__":
    trig_off=np.ones(20)
    trig_on=np.ones(5)*5
    trig_signal=trig_off
    for i in range(3):
        trig_signal=np.append(trig_signal,trig_on)
        trig_signal=np.append(trig_signal,trig_off)
    print "trigger signal"
    print trig_signal
    
    chan_num=2    
    
    raw_signal=np.transpose(np.vstack([np.ones(chan_num) for a in range(len(trig_signal))]))
#    channels_signal=np.random.random((8,len(trig_signal)))
    print "raw_signal"
    print raw_signal
    print
    
    integral_zero=np.zeros(chan_num)
    print integral_zero
    integral=integral_zero
    integrated_signal=[]
    trig_cutoff=2
    t0=time.clock()
    for d,t in zip(np.transpose(raw_signal),trig_signal):
        if t>trig_cutoff:
            if not (integral==integral_zero).all():
                integrated_signal.append(integral)
                integral=integral_zero
        else:
            integral=integral+d
    integrated_signal=np.transpose(np.vstack(integrated_signal))
    print "it took ",time.clock()-t0,"seconds to run this function"
    
    t0=time.clock()
    indexes=get_triggered_indexes(trig_signal,2)
    print    
    print "integrated_signal"
    print integrate_signal(raw_signal, indexes)
    print time.clock()-t0
    
    