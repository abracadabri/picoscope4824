# -*- coding: utf-8 -*-
"""
Created on Sun Nov 30 23:27:33 2014

@author: pfduc
"""

from numpy import arange,mod
fs=35
fsl=25
marker_size=18

marker_set=["None","o","v","^",">","<","s","d","h","x","+","|","_"]
line_set=['-', '--', '-.', ':','None']


def color_blind_friendly_colors(nb_lines=None):
    if nb_lines==1:
        return ['#4477AA']
    elif nb_lines==2:
        return ['#4477AA','#CC6677']
    elif nb_lines==3:
        return ["#4477AA","#DDCC77","#CC6677"]
    elif nb_lines==4:
        return ["#4477AA","#117733","#DDCC77","#CC6677"]
    elif nb_lines==5:
        return ['#332288','#88CCEE','#117733','#DDCC77','#CC6677']
    elif nb_lines==6:
        return ['#332288','#88CCEE','#117733','#DDCC77','#CC6677','#AA4499']
    elif nb_lines==7:
        return ['#332288','#88CCEE','#44AA99','#117733','#DDCC77','#CC6677','#AA4499']
    elif nb_lines==8:
        return ['#332288','#88CCEE','#44AA99','#117733','#999933','#DDCC77','#CC6677','#AA4499']
    elif nb_lines==9:
        return ['#332288','#88CCEE','#44AA99','#117733','#999933','#DDCC77','#CC6677','#882255','#AA4499']
    elif nb_lines==10:
        return ['#332288','#88CCEE','#44AA99','#117733','#999933','#DDCC77','#661100','#CC6677','#882255','#AA4499']
    elif nb_lines==11:
        return ['#332288','#6699CC','#88CCEE','#44AA99','#117733','#999933','#DDCC77','#661100','#CC6677','#882255','#AA4499']
    else:
        return ['#332288','#6699CC','#88CCEE','#44AA99','#117733','#999933','#DDCC77','#661100','#CC6677','#AA4466','#882255','#AA4499']
