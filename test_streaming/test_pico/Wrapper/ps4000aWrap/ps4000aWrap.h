/****************************************************************************
 *
 * Filename:    ps4000aWrap.h
 * Copyright:   Pico Technology Limited 2014
 * Author:      HSM
 * Description:
 *
 * This header defines the interface to the wrapper dll for the 
 *	PicoScope 4000 series of PC Oscilloscopes using the PicoScope 4000 
 *  Series 'A' API.
 *
 ****************************************************************************/
#ifndef __PS4000AWRAP_H__
#define __PS4000AWRAP_H__

#include "ps4000aApi.h"

extern PICO_STATUS __declspec(dllexport) __stdcall RunBlock
(
	int16_t handle, 
	int32_t preTriggerSamples, 
	int32_t postTriggerSamples, 
	uint32_t timebase,
	uint32_t segmentIndex
);

extern PICO_STATUS __declspec(dllexport) __stdcall GetStreamingLatestValues
(
	int16_t handle
);

extern uint32_t __declspec(dllexport) __stdcall AvailableData
(
	int16_t handle, 
	uint32_t *startIndex
);

extern int16_t __declspec(dllexport) __stdcall AutoStopped
(
	int16_t handle
);

extern int16_t __declspec(dllexport) __stdcall IsReady
(
	int16_t handle
);

extern int16_t __declspec(dllexport) __stdcall IsTriggerReady
(
	int16_t handle, 
	uint32_t *triggeredAt
);

extern int16_t __declspec(dllexport) __stdcall ClearTriggerReady
(
	int16_t handle
);

extern void __declspec(dllexport) __stdcall setChannelCount
(
	int16_t handle, 
	int16_t channelCount
);

extern int16_t __declspec(dllexport) __stdcall setEnabledChannels
(
	int16_t handle, 
	int16_t * enabledChannels
);

extern int16_t __declspec(dllexport) __stdcall setAppAndDriverBuffers
(
	int16_t handle, 
	int16_t channel, 
	int16_t * appBuffer, 
	int16_t * driverBuffer,
	int32_t bufferLength
);

extern int16_t __declspec(dllexport) __stdcall setMaxMinAppAndDriverBuffers
(
	int16_t handle, 
	int16_t channel, 
	int16_t * appMaxBuffer,
	int16_t * appMinBuffer, 
	int16_t * driverMaxBuffer, 
	int16_t * driverMinBuffer,
	int32_t bufferLength
);

#endif
