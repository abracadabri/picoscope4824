/**************************************************************************
*
* Filename:    PS4000aImports.cs
*
* Copyright:   Pico Technology Limited 2014
*
* Author:      KPV
*
* Description:
*   This file contains all the .NET wrapper calls needed to support
*   the console example. It also has the enums and structs required
*   by the (wrapped) function calls.
*
* History:
*    05	KPV	Created
*
* Revision Info: "file %n date %f revision %v"
*						""
*
***************************************************************************/

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace PS4000A_sigAWG
{
    class Imports
    {
        #region constants
        private const string _DRIVER_FILENAME = "ps4000A.dll";

        public const int MaxValue = 32767;

        public const int SIG_GEN_BUFFER_SIZE = 16384;

        public const Int64 AWG_PHASE_ACCUMULATOR = 4294967296;

        public const int AWG_DAC_FREQUENCY = 80000000;

        #endregion

        #region Driver enums

        public enum coupling : int
        {
            AC,
            DC
        }

        public enum Channel : int
        {
            CHANNEL_A,
            CHANNEL_B,
            CHANNEL_C,
            CHANNEL_D,
            MAX_4_CHANNELS,
            CHANNEL_E = MAX_4_CHANNELS,
            CHANNEL_F,
            CHANNEL_G,
            CHANNEL_H,
            EXTERNAL,
            MAX_CHANNELS = EXTERNAL,
            TRIGGER_AUX,
            MAX_TRIGGER_SOURCES,
            PULSE_WIDTH_SOURCE = 0x10000000
        }

        public enum Range : int
        {
            Range_10MV,
            Range_20MV,
            Range_50MV,
            Range_100MV,
            Range_200MV,
            Range_500MV,
            Range_1V,
            Range_2V,
            Range_5V,
            Range_10V,
            Range_20V,
            Range_50V,
            Range_100V
        }

        public enum ReportedTimeUnits : int
        {
            FemtoSeconds,
            PicoSeconds,
            NanoSeconds,
            MicroSeconds,
            MilliSeconds,
            Seconds
        }

        public enum ThresholdMode : int
        {
            Level,
            Window
        }

        public enum ThresholdDirection : int
        {
            // Values for level threshold mode
            //
            Above,
            Below,
            Rising,
            Falling,
            RisingOrFalling,

            // Values for window threshold mode
            //
            Inside = Above,
            Outside = Below,
            Enter = Rising,
            Exit = Falling,
            EnterOrExit = RisingOrFalling,

            None = Rising
        }

        public enum DownSamplingMode : int
        {
            None,
            Aggregate,
            Decimate,
            Average
        }

        public enum PulseWidthType : int
        {
            None,
            LessThan,
            GreaterThan,
            InRange,
            OutOfRange
        }

        public enum TriggerState : int
        {
            DontCare,
            True,
            False
        }

        public enum Model : int
        {
            NONE = 0,
            PS4824 = 4824
        }

        public enum INFO : int
        {
            Clear,
            Add
        }

        public enum IndexMode : int
        {
            PS4000A_SINGLE,
            PS4000A_DUAL,
            PS4000A_QUAD,
            PS4000A_MAX_INDEX_MODES
        }

        public enum WaveType : int
        {
            PS4000A_SINE,
            PS4000A_SQUARE,
            PS4000A_TRIANGLE,
            PS4000A_RAMP_UP,
            PS4000A_RAMP_DOWN,
            PS4000A_SINC,
            PS4000A_GAUSSIAN,
            PS4000A_HALF_SINE,
            PS4000A_DC_VOLTAGE,
            PS4000A_WHITE_NOISE,
            PS4000A_MAX_WAVE_TYPES
        }

        public enum SweepType : int
        {
            PS4000A_UP,
            PS4000A_DOWN,
            PS4000A_UPDOWN,
            PS4000A_DOWNUP,
            PS4000A_MAX_SWEEP_TYPES
        }

        public enum ExtraOperations : int
        {
            PS4000A_ES_OFF,
            PS4000A_WHITENOISE,
            PS4000A_PRBS // Pseudo-Random Bit Stream 
        }

        public enum SigGenTrigType : int
        {
            PS4000A_SIGGEN_RISING,
            PS4000A_SIGGEN_FALLING,
            PS4000A_SIGGEN_GATE_HIGH,
            PS4000A_SIGGEN_GATE_LOW
        }

        public enum SigGenTrigSource : int
        {
            PS4000A_SIGGEN_NONE,
            PS4000A_SIGGEN_SCOPE_TRIG,
            PS4000A_SIGGEN_AUX_IN,
            PS4000A_SIGGEN_EXT_IN,
            PS4000A_SIGGEN_SOFT_TRIG
        }
        #endregion

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriggerChannelProperties
        {
            public Int16 ThresholdMajor;
            public UInt16 HysteresisMajor;
            public Int16 ThresholdMinor;
            public UInt16 HysteresisMinor;
            public Channel Channel;
            public ThresholdMode ThresholdMode;


            public TriggerChannelProperties(
                Int16 thresholdMajor,
                UInt16 hysteresisMajor,
                Int16 thresholdMinor,
                UInt16 hysteresisMinor,
                Channel channel,
                ThresholdMode thresholdMode)
            {
                this.ThresholdMajor = thresholdMajor;
                this.HysteresisMajor = hysteresisMajor;
                this.ThresholdMinor = thresholdMinor;
                this.HysteresisMinor = hysteresisMinor;
                this.Channel = channel;
                this.ThresholdMode = thresholdMode;
            }
        }



        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriggerConditions
        {
            public Channel Source;
            public TriggerState Condition;

            public TriggerConditions(
                Channel source,
                TriggerState condition)
            {
                this.Source = source;
                this.Condition = condition;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TriggerDirections
        {
            public Channel Source;
            public ThresholdDirection Direction;

            public TriggerDirections(
                Channel source,
                ThresholdDirection direction)
            {
                this.Source = source;
                this.Direction = direction;
            }
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PwqConditions
        {
            public TriggerState ChannelA;
            public TriggerState ChannelB;
            public TriggerState ChannelC;
            public TriggerState ChannelD;
            public TriggerState ChannelE;
            public TriggerState ChannelF;
            public TriggerState ChannelG;
            public TriggerState ChannelH;
            public TriggerState External;
            public TriggerState Aux;

            public PwqConditions(
                TriggerState channelA,
                TriggerState channelB,
                TriggerState channelC,
                TriggerState channelD,
                TriggerState channelE,
                TriggerState channelF,
                TriggerState channelG,
                TriggerState channelH,
                TriggerState external,
                TriggerState aux)
            {
                this.ChannelA = channelA;
                this.ChannelB = channelB;
                this.ChannelC = channelC;
                this.ChannelD = channelD;
                this.ChannelE = channelE;
                this.ChannelF = channelF;
                this.ChannelG = channelG;
                this.ChannelH = channelH;
                this.External = external;
                this.Aux = aux;
            }
        }

        #region Driver Imports
        #region Callback delegates
        public delegate void ps4000aBlockReady(short handle, short status, IntPtr pVoid);

        public delegate void ps4000aStreamingReady(
                                                short handle,
                                                int noOfSamples,
                                                uint startIndex,
                                                short overflow,
                                                uint triggerAt,
                                                short triggered,
                                                short autoStop,
                                                IntPtr pVoid);

        public delegate void ps4000aDataReady(
                                                short handle,
                                                UInt32 status,
                                                int noOfSamples,
                                                short overflow,
                                                IntPtr pVoid);
        #endregion

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aOpenUnit")]
        public static extern Int32 OpenUnit(out short handle, StringBuilder serial);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aCloseUnit")]
        public static extern Int32 CloseUnit(short handle);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aRunBlock")]
        public static extern Int32 RunBlock(
                                                short handle,
                                                int noOfPreTriggerSamples,
                                                int noOfPostTriggerSamples,
                                                uint timebase,
                                                out int timeIndisposedMs,
                                                ushort segmentIndex,
                                                ps4000aBlockReady lpps4000aBlockReady,
                                                IntPtr pVoid);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aChangePowerSource")]
        public static extern Int32 ps4000aChangePowerSource(short handle, Int32 status);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aStop")]
        public static extern Int32 Stop(short handle);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aSetChannel")]
        public static extern Int32 SetChannel(
                                                short handle,
                                                Channel channel,
                                                short enabled,
                                                short dc,
                                                Range range,
                                                float analogOffset);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aSetDataBuffers")]
        public static extern Int32 SetDataBuffers(
                                                short handle,
                                                Channel channel,
                                                short[] bufferMax,
                                                short[] bufferMin,
                                                int bufferLth,
                                                uint segmentIndex,
                                                DownSamplingMode RatioMode);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aSetTriggerChannelDirections")]
        public static extern Int32 SetTriggerChannelDirections(
                                                short handle,
                                                ref TriggerDirections[] directions,
                                                short nDirections);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aGetTimebase")]
        public static extern Int32 GetTimebase(
                                             short handle,
                                             uint timebase,
                                             int noSamples,
                                             out int timeIntervalNanoseconds,
                                             out int maxSamples,
                                             ushort segmentIndex);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aGetValues")]
        public static extern Int32 GetValues(
                short handle,
                uint startIndex,
                ref uint noOfSamples,
                uint downSampleRatio,
                DownSamplingMode downSampleDownSamplingMode,
                ushort segmentIndex,
                out short overflow);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aSetTriggerChannelProperties")]
        public static extern Int32 SetTriggerChannelProperties(
            short handle,
            ref TriggerChannelProperties[] channelProperties,
            short numChannelProperties,
            short auxOutputEnable,
            int autoTriggerMilliseconds);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aSetTriggerChannelConditions")]
        public static extern Int32 SetTriggerChannelConditions(
            short handle,
            ref TriggerConditions[] conditions,
            short numConditions,
            INFO info);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aSetTriggerDelay")]
        public static extern Int32 SetTriggerDelay(short handle, uint delay);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aSetSimpleTrigger")]
        public static extern Int32 SetSimpleTrigger(
            short handle,
            short enable,
            Channel channel,
            short threshold,
            ThresholdDirection direction,
            int delay,
            short autotrigger_ms);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aGetUnitInfo")]
        public static extern Int32 GetUnitInfo(short handle, StringBuilder infoString, short stringLength, out short requiredSize, int info);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aRunStreaming")]
        public static extern Int32 RunStreaming(
            short handle,
            ref uint sampleInterval,
            ReportedTimeUnits sampleIntervalTimeUnits,
            uint maxPreTriggerSamples,
            uint maxPostPreTriggerSamples,
            bool autoStop,
            int downsampleratio,
            DownSamplingMode downSamplingRatioMode,
            uint overviewBufferSize);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aGetStreamingLatestValues")]
        public static extern Int32 GetStreamingLatestValues(
            short handle,
            ps4000aStreamingReady lpps4000aStreamingReady,
            IntPtr pVoid);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aSetNoOfCaptures")]
        public static extern Int32 SetNoOfRapidCaptures(
            short handle,
            uint nWaveforms);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aMemorySegments")]
        public static extern Int32 MemorySegments(
            short handle,
            ushort nSegments,
            out int nMaxSamples);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aSetDataBufferBulk")]
        public static extern Int32 SetDataBuffersRapid(
            short handle,
            Channel channel,
            short[] buffer,
            int bufferLth,
            ushort waveform);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aGetValuesBulk")]
        public static extern Int32 GetValuesRapid(
            short handle,
            ref uint noOfSamples,
            ushort fromSegmentIndex,
            ushort toSegmentIndex,
            int downsampleratio,
            DownSamplingMode downsampleratiomode,
            short[] overflows);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aMaximumValue")]
        public static extern Int32 MaximumValue(
            short handle,
         out short value);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aSetSigGenArbitrary")]
        public static extern Int32 SetSigGenArbitray(
            short handle,
         int offsetVoltage,
            uint pkTopk,
          uint StartDeltaPhase,
            uint StopDeltaPhase,
            uint deltaPhaseIncrement,
            uint dwellCount,
           ref short arbitaryWaveform,
            int arbitaryWaveformSize,
        SweepType sweepType,
            ExtraOperations operation,
            IndexMode indexMode,
            uint shots,
        uint sweeps,
            SigGenTrigType triggerType,
            SigGenTrigSource triggerSource,
            short extInThreshold);

        [DllImport(_DRIVER_FILENAME, EntryPoint = "ps4000aSetSigGenBuiltIn")]
        public static extern Int32 SetSigGenBuiltIn(
            short handle,
         int offsetVoltage,
            uint pkTopk,
            WaveType waveType,
          double StartFrequency,
           double StopFrequency,
            double Increment,
            double dwellTime,
        SweepType sweepType,
            ExtraOperations operation,
            uint shots,
        uint sweeps,
            SigGenTrigType triggerType,
            SigGenTrigSource triggerSource,
            short extInThreshold);
        #endregion
    }
}
