# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 13:08:28 2015

@author: Helene
"""

# -*- coding: utf-8 -*-
"""
Created on Sun Nov 09 11:00:38 2014

@author: Helene
"""

import sys
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QReadWriteLock
from ctypes import byref, POINTER, create_string_buffer, c_float, \
    c_int16, c_int32, c_uint32, c_void_p, CFUNCTYPE


import time
import numpy as np
import ps4000a
import pyqt_window as pyqt_window


class PicoMainWidget(QtGui.QWidget):
     def __init__(self,parent=None):
        
        super(PicoMainWidget, self).__init__(parent)
        
        self.debug= False
        
        self.connect_instr()
        self.idx=0
        self.n_active_channels=2
        self.channel_number=1
        self.obs_duration=40E-3
        self.sampling_interval=5E-6
        
        if self.debug==False:
            self.acq_duration=1
            self.laser_rep_period= 0.0005
            self.set_channels_properties()
            self.set_trigger_conditions()
            self.lock = QReadWriteLock()
            self.data_array=np.array([])
            self.datataker = DataTaker(self.lock,self.scope, self.n_active_channels,self)
            self.set_acquisition_parameters(self.obs_duration,self.sampling_interval)
            #self.connect(self.datataker, QtCore.SIGNAL("data(PyQt_PyObject)"), self.plot_spectrum)
            #self.connect(self.datataker, QtCore.SIGNAL("data(PyQt_PyObject)"), self.integrate)
            #trig_enabled=0
            #pulse_width=0
            #print("what is returned by this function")
            #print(self.scope.lib.ps4000aIsTriggerOrPulseWidthQualifierEnabled(c_int16(self.scope.handle), c_int16(trig_enabled), c_int16(pulse_width)))
            #print "The value of the trigger is"        
            #print(trig_enabled)

        else:
            self.scope=None
            self.lock = QReadWriteLock()
            self.datataker = DataTaker(self.lock,self.scope, self.n_active_channels,self)
            self.actualSamplingInterval=1
            self.n_active_channels=8
        #Everything that has to do with the Menu        
        self.myQMenuBar = QtGui.QMenuBar(self)   

        exitMenu = self.myQMenuBar.addMenu('File')
        exitAction = QtGui.QAction('Exit', self)        
        exitAction.triggered.connect(QtGui.qApp.quit)
        exitMenu.addAction(exitAction)
        self.win=pyqt_window.PyQtGraphWidget(n_curves=4)
        
        #Here need to replace the 2000 with variables. 
        self.win.setYRange(0,2000)
        self.win.setTitle("Spectrum")
        self.win.setLabel('left', 'Voltage', units='V')
        self.win.setLabel('bottom', 'Shots', units='')
        
        #Creates button "Acquire"
        self.acquire_button= QtGui.QPushButton(self)
        self.acquire_button.setText("Acquire")
        self.acquire_button.setFixedHeight(30)  
        self.acquire_button.setFixedWidth(150)
        self.acquire_continuous_tick=QtGui.QCheckBox("Continuous")
        self.acquire_button.clicked.connect(self.start_acquisition)
        
        #Creates "stop button"
        self.stop_button= QtGui.QPushButton(self)  
        self.stop_button.setText("Stop")
        self.stop_button.setFixedHeight(30)  
        self.stop_button.setFixedWidth(150)
        self.stop_button.setEnabled(False)
        self.stop_button.clicked.connect(self.stop_acquisition)
        
        #LAYOUT STUFF  
        # Grid 1 contains the acquire, background and stop buttons
        self.Grid1=QtGui.QGridLayout()
        self.Grid1.setColumnStretch(0,1)
        self.Grid1.addWidget(self.acquire_button,0,0,  QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.acquire_continuous_tick,0,1, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.stop_button,2,0, QtCore.Qt.AlignVCenter)
        self.Grid1.setRowMinimumHeight(1,self.acquire_button.height())
        self.Grid1.setRowMinimumHeight(2,self.stop_button.height())
        
        #Vlayout is the main layout
        self.Vlayout=QtGui.QVBoxLayout()
        self.Vlayout.addWidget(self.myQMenuBar)
        self.Vlayout.addLayout(self.Grid1)
        self.Vlayout.addWidget(self.win)
        self.setLayout(self.Vlayout)
        
        #Window properties are set here
        self.setGeometry(500, 500, 450, 650)
        self.setMaximumHeight(1000)
        self.setMaximumWidth(700)
        self.setWindowTitle('PicoMainWidget')
        QtGui.QApplication.setStyle("windowsxp")
               
        #Creates a QTimer used in the continuous plotting mode for updating the plot
        self.refresh=30
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.plot_spectrum)
        
        self.show()
     
     def connect_instr(self):
        print("Attempting to open Picoscope 4824...")
        if self.debug:
            print "="*20
            print "Debug Mode"
            self.scope=None
            self.nSamples=100
        else:
            self.scope = ps4000a.PS4000a(debug=self.debug)
            self.scope.flashLed(5)
        print("Found the following picoscope:")
        #print(self.scope.getAllUnitInfo())   
        
     def set_channels_properties(self):
        
         for i in range(self.channel_number):
             channelRange = self.scope.setChannel(i, 'DC', 5.0, 0.0, enabled=True, BWLimited=True)
             print("Chosen channel range = %d" % channelRange)
             
     def set_trigger_conditions(self):
        self.scope.setSimpleTrigger('A', 0.21, "Rising", delay=0, timeout_ms=10000, enabled=True)
        print "Setting the trigger properly"
        
        
        
     def set_acquisition_parameters(self, obs_duration, sampling_interval):
      
        if self.debug == False:
            (self.actualSamplingInterval, self.nSamples, self.maxSamples) = \
            self.scope.setSamplingInterval(sampling_interval, obs_duration)
            print("Initial Sampling interval = %f ns" % (sampling_interval* 1E9 ))           
            print("Sampling interval = %f ns" % (self.actualSamplingInterval * 1E9))
            print("Taking  samples = %d" % self.nSamples)
            print("Maximum samples = %d" % self.maxSamples)
            print("Handle is")
            print(self.scope.handle)
        self.datataker.set_acquisition_parameters(obs_duration, self.actualSamplingInterval, self.nSamples)
    
       # stop_acquisition is used for single spectrum acquisition, continuous mode
       # and background acquisition
     def stop_acquisition(self):         
        if not self.datataker.isStopped():  
            self.acquire_button.setEnabled(True)
            self.stop_button.setEnabled(False)
            self.datataker.resume()
            self.datataker.stop()
            print "acquisition STOPPED"

        else:         
            print "Couldn't stop acquisition - it wasn't running!"
    
       # start_acquisition is used for single spectrum acquisition and continuous mode
     def start_acquisition(self):

        try:
            self.acquire_button.setEnabled(False)
            self.stop_button.setEnabled(True)
            if self.datataker.isStopped():
                #self.spectro.prepare_measurement()
                #self.spectro_thread.update_spectro_settings(self.spectro.exposure,self.nshots)
                
             
                self.datataker.start()
                self.timer.start(self.refresh)
            else:         
                print "Couldn't start acquisition - already running!"
            
        except USBError:
               self.pop_up= QtGui.QMessageBox()
               self.pop_up.setText("USBError: check that cable is plugged")
               self.pop_up.exec_()
               
     def plot_spectrum(self):
          self.idiot=0
          #self.data= np.array(self.datataker.dataV)
          #print "This is the shape of the data"
          #print self.data
          #print(len(self.datataker.dataV[0]))
          #print(len(self.datataker.dataV[1]))
          
          #time_axis=np.arange(len(data_set[0]))
          #self.win.update_plot(time_axis, data_set[0],0)
          #self.win.update_plot(time_axis, data_set[1],1)
          time_axis=np.arange(len(self.datataker.dataV[0]))
          self.win.update_plot(time_axis, self.datataker.dataV[0],0)
          self.win.update_plot(time_axis, self.datataker.dataV[1],1)
          #print "will plot spectrum"
               
class DataTaker(QtCore.QThread):
    
       
    def __init__(self, lock,instrument, n_active_channels, parent=None):
        print "Pico created"
        super(DataTaker, self).__init__(parent)

        """
        COMMENT: here instrument will be from the class picoscope
        """
        self.instrument= instrument
        #self.nsamples= nsamples
        self.n_active_channels= n_active_channels
        self.lock = lock
        self.stopped = True
        self.paused = False
        self.mutex = QtCore.QMutex()
        
        self.dataV=[]
        #change for active channels here
        for i in range(2):
            self.dataV.append(np.zeros(1,dtype=np.int16))
            
        self.completed = False

  
        
    
    def __del__(self):
        print "Pico deleted"

 
    def set_acquisition_parameters(self,dt,sampling_interval, nsamples):
        self.observation_duration=dt
        self.nsamples=nsamples
        self.sampling_interval=sampling_interval
 
    def run(self):
        print "Pico begin run"
        self.stopped = False

        #while self.stopped==False:

        self.read_data()  
        """        
        data_set=[]
        self.instrument.runBlock()   
        print "Here"             
        self.instrument.waitReady()
        print "Heree"
        for i in range(self.n_active_channels):
            data_set.append(self.instrument.getDataV(i, self.nsamples,returnOverflow=False))
                    
        print "Heree"
        self.emit(QtCore.SIGNAL("data(PyQt_PyObject)"),data_set)
            
        self.completed=True
        self.emit(QtCore.SIGNAL("script_finished(bool)"), self.completed)        
        self.stopped = True        
        """
        print "Pico run is over"    
        
        
    def stop(self):
        try:
            self.mutex.lock()
            self.stopped = True
            print "Pico stopped"
        finally:
            self.mutex.unlock()

    def pause(self):
        print "Pico paused"
        self.paused = True
            
    def resume(self,display=True):
        if display:
            print "Pico resumed"
        self.paused = False

    def isPaused(self): 
        return self.paused
            
    def isStopped(self): 
        return self.stopped
            
            
    def check_stopped_or_paused(self):
        while True:            
            if (not self.paused) or self.stopped:
                return self.stopped
            time.sleep(0.1)
    

    
    def read_data(self):
        """
            Call the method "measure" for each instrument in InstrumentHub.
            It collect the different values of corresponding parameters and
            emit a signal which will be catch by other instance for further
            treatment.
        """            

        self.stopped=False
        #print "This is the actual number of samples"
        #print self.nsamples
        #self.data_buffer=[]
        #for i in range(2):
        #    self.data_buffer.append(np.empty(self.nsamples, dtype=np.int16))
        """
        data_set=[]
        print "test1"
        self.instrument.runBlock()   
        print "test2"             
        self.instrument.waitReady()
        print "test3"
        for i in range(self.n_active_channels):
            data_set.append(self.instrument.getDataV(i, self.nsamples,returnOverflow=False))
        self.dataV=[]
        print "test4"
        self.emit(QtCore.SIGNAL("data(PyQt_PyObject)"), np.array(data_set))
        print "test5"
        """
        
        self.g_ready = False
        self.g_sampleCount = 0
        self.g_startIndex=0
        self.g_autoStop=1
        self.g_trig = 0
        self.g_trigAt = 0
            
        pretrig=0
        downSampleRatio=1
        downSampleMode=0
        segmentIndex=0

        self.data_buffer1= (np.empty(self.nsamples, dtype=np.int16))
        self.data_buffer2=(np.empty(self.nsamples, dtype=np.int16))
        self.instrument._lowLevelSetDataBuffer(0, self.data_buffer1, downSampleMode, segmentIndex)
        self.instrument._lowLevelSetDataBuffer(1, self.data_buffer2, downSampleMode, segmentIndex)

        self.instrument.setSimpleTrigger('A', 1, "Rising", delay=0, timeout_ms=10000, enabled=True)
        
        sampleInterval=20
        
        ps4000aStreamingReady = CFUNCTYPE(c_void_p,c_int16, c_uint32, c_uint32, c_int16, c_uint32, c_int16,
                   c_int16, c_void_p)    
        
        #ps4000aStreamingReady = CFUNCTYPE(c_void_p,c_uint32)
                 
        data_buffer_app=[] #deque(maxlen=1000000)
        self.has_recieved_data=0

                          
        cmp_func= ps4000aStreamingReady(self.CallBackStreaming)

        while self.stopped==False:
        #for i in range(1000):
            #time.sleep(0.005)  
            print("nsamples before going in the callback")
            print(int(self.nsamples*(1-pretrig)))
            self.instrument._lowLevelRunStreaming(int(self.sampling_interval*1e06), 3, int(self.nsamples * pretrig),
                          int(self.nsamples * (0.5 - pretrig)), downSampleRatio,
                            downSampleMode, len(self.data_buffer1))
                            
            while self.has_recieved_data==0:
               time.sleep(0.1)
               self.instrument.lib.ps4000aGetStreamingLatestValues(c_int16(self.instrument.handle), cmp_func, c_void_p())
            
            self.has_recieved_data=0
            #self.instrument._lowLevelGetStreamingLatestValues()                                    
            
         
            
        self.instrument.stop()
        for i in range(8):
             self.instrument._lowLevelClearDataBuffer(i,0)
             
    def CallBackStreaming(self, handle,noSamples,startIndex,overflow,triggerAt, triggered, autoStop, pParameter):
#                          t0 = time.clock()
        self.g_sampleCount = noSamples
        self.g_startIndex = startIndex
        self.g_autoStop = autoStop

        #flag to say done reading data
        self.g_ready = True
        #flags to show if & where a trigger has occurred
        self.g_trig = triggered
        self.g_trigAt = triggerAt                       
        
        """
        print("handle")
        print(handle)
        print("nosamples:")
        print(noSamples)
        print("startIndex")
        print(startIndex)
        print("overflow")
        print(overflow)
        """
        print("Trigger at")
        print(triggerAt)
        print("Triggered")
        print(triggered)
        """        
        print("Autostop")
        print(autoStop)
        print("pParameter")
        print(pParameter)
        """
        #print(self.instrument.handle())
        self.has_recieved_data=1
        if triggered==1:
            self.dataV[0]=(self.instrument.rawToV(0,self.data_buffer1))[triggerAt: triggerAt + 1000]
            self.dataV[1]=(self.instrument.rawToV(1,self.data_buffer2))[triggerAt: triggerAt + 1000]
            #print "are we going in the Callback Function"
            #data_buffer_app.append(dataV)
            self.emit(QtCore.SIGNAL("data(PyQt_PyObject)"), self.dataV)
        #print "are we going in the Callback Function"
                              
def main():
    
    app = QtGui.QApplication(sys.argv)
    ex = PicoMainWidget()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()