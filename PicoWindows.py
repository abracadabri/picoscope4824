# -*- coding: utf-8 -*-
"""
Created on Fri Nov 07 08:58:39 2014

@author: Helene
"""

import sys
from PyQt4 import QtGui, QtCore
import numpy as np
#import time
#import cPickle
import ui_PlotDisplayWindow
from collections import OrderedDict
import QtTools
from matplotlib import dates
from matplotlib import ticker

import datetime


from PlotingOptions import color_blind_friendly_colors,marker_set,line_set
from mplZoomWidget import MatplotlibZoomWidget
from matplotlib import ticker


# Not sure what this is for - it's copy-paste from some example.
try:
    from PyQt4.QtCore import QString
except ImportError:
    QString = str
try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s



chan_contr=OrderedDict()
chan_contr["groupBox_Name"]=["Channel","lineEdit"]
#chan_contr["groupBox_X"]=["X","radioButton"]
chan_contr["groupBox_Y"]=["Show","checkBox"]
#chan_contr["groupBox_YR"]=["Y","checkBox"]
#chan_contr["groupBox_invert"]= ["+/-","checkBox"]
chan_contr["groupBox_color"]= ["Col","colorButton"]
#chan_contr["groupBox_marker"]= ["M","comboBox"]
#chan_contr["groupBox_line"]= ["L","comboBox"]

def get_groupBox_purpouse(name):
    return name.split("_")[1]


import random

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
import matplotlib.pyplot as plt

class MatplotlibWidget(QtGui.QWidget):
    def __init__(self, parent=None,channel_number=8,labels=None):
        super(MatplotlibWidget, self).__init__(parent)

        self.figure = plt.figure()
        self.canvas = FigureCanvasQTAgg(self.figure)
        self.figure.subplots_adjust(bottom=0.2)

        self.axis = self.figure.add_subplot(111)
        
        for i in range(channel_number):
            if labels==None:
                name="channel %i"%(i+1)
            else:
                name=labels[i]
            self.axis.plot([],[],label=name)
            
        self.axis.legend(prop={"size":10})
        self.axis.legend(loc=2, frameon=False)
        self.toolbar = NavigationToolbar(self.canvas, self)
        #prop={"size":15} a mettre dans legend pour changer la taille
        
        #create a number of lines (which are blank at first) equal to the the number of channel on the scope
        #for i in range(channel_number):
        """
            decommente la ligne juste en dessous et commente les deux lignes qui suivent
        """
#            self.axis.plot([],[])

            #juste une example, a l'initialisation tu 
         #   x=arange(1,10,1)
          #  self.axis.plot(x,(x**2)+i)
        
        """
        fonctions utiles, il te faut seulement redefinir une fonction qui prend en argument les nouvelles donnes x et y
        ainsi que le channel number, comme ca tu n'a pas besoin d'appeler "self.axis.lines[0].set_data" depuis ailleurs
        mais juste 
        set_data(chan_num,data_array)
        """
        #acces aux donnees de la ligne 1
      #  print self.axis.lines[1].get_data()
        #change les données de la ligne 0
       # self.axis.lines[0].set_data([1,2,3,4],[1,2,4,8])
            
        #self.axis.set_axes('xlabel')
        #self.axis.set_axes('ylabel')
        
    
        #matplotlib.axes.Axes.set_xlabel() 

        self.layoutVertical = QtGui.QVBoxLayout(self)
        self.layoutVertical.addWidget(self.toolbar)
        self.layoutVertical.addWidget(self.canvas)
       

        #layout.addWidget(self.button)
        self.setLayout(self.layoutVertical)
    
    def change_x_axis_label(self, new_label_x):
        
        self.x_axes= plt.xlabel(new_label_x)
    
    def change_y_axis_label(self, new_label_y):
        
        self.y_axes= plt.ylabel(new_label_y)
    
    def set_data(self, channel_no,data_x, data_y=None,):
        if data_y == None:
            data_y= data_x
            data_x=np.arange(len(data_y))
#        else:
            #self.axis.axis([x_min, x_max, y_min, y_max])
        self.axis.lines[channel_no].set_data(data_x,data_y)
            #self.set_axis_scale()            
            #self.axis.axis([np.min(data_x), np.max(data_x), np.min(data_y), np.max(data_y)])
        self.canvas.draw()
        self.canvas.show()
    
    def get_data(self,channel_no=0):
        return self.axis.lines[channel_no].get_data()
            
    def axis_scale(self, x_min, x_max, y_min, y_max):
        self.axis.axis([x_min, x_max,y_min, y_max])
        self.canvas.draw()
        self.canvas.show()
            
    def set_linestyle(self,channel_no,linestyle):
        self.axis.lines[channel_no].setp(ls=linestyle)
        
    def draw_circle(self):
        circle=plt.Circle((0,0),1, color='0.85')
        self.axis.add_patch(circle)
        self.canvas.draw()
        self.canvas.show()
        
    def turn_grid_on(self):
        plt.grid()
        self.canvas.draw()
        self.canvas.show()
        
    def clear_data(self,channel_no):
        self.set_data(channel_no,[])
        
    def draw_line_x(self,x_pos):
        
           self.lx_max = self.axis.axvline(x=x_pos, color='r')
           self.lx_max.set_linestyle('--')
           self.canvas.draw()
           self.canvas.show()
           
    def draw_line_y(self,y_pos):
           self.ly_max = self.axis.axhline(y=y_pos, color='r')
           self.ly_max.set_linestyle('--')
           self.canvas.draw()
           self.canvas.show()

class ScopeScreen(QtGui.QWidget):
    def __init__(self, parent=None,channel_number=8,labels=None,ofname=None,track_signal=False):
        super(ScopeScreen, self).__init__(parent)
#        if not ofname==None:
#            self.of=open(ofname,'a')
##            self.of=ofname
#        else:
#            self.of=None
        
        self.num_channel=channel_number        
        
        if track_signal==True:
            self.connect(parent,QtCore.SIGNAL("update_plot(PyQt_PyObject)"),self.update_plot)
        
        self.matplotlibWidget = MatplotlibWidget(self,channel_number,labels)

        self.layoutVertical = QtGui.QVBoxLayout(self)
#        self.layoutVertical.addWidget(self.pushButtonPlot)
        self.layoutVertical.addWidget(self.matplotlibWidget)
        
        for i in range(channel_number):
            print i
            self.plot_data_array(i,np.arange(10)*(i+1))
        

        #self.threadSample = ThreadSample(self)
        #self.threadSample.newSample.connect(self.on_threadSample_newSample)
        #self.threadSample.finished.connect(self.on_threadSample_finished)

    def __del__(self):
        super(ScopeScreen, self).__del__()
        if not self.of==None:
            self.of.close()

    
    def update_plot(self,new_data_set):
        print "the data is updated in the plotter"

        #t=new_data_set[0]
        
        #dt = time.clock()-t
        #print dt
        #t0=time.clock()
        #inf=open("C:\Users\Helene\Documents\Work\McGill\PhD\Code\Picoscope4824_BitBucket\streamdata_from_datataker.dat",'r')
        #print "PLOT : time for opening a file stream in readmode",time.clock()-t0
        #t0=time.clock()
        #data=cPickle.load(inf)
        #print "PLOT : time loading the data",time.clock()-t0
        #print data
        #t0=time.clock()
        self.plot_data_array(new_data_set)
        #print "PLOT : time for plot and rescale",time.clock()-t0
        #tu ne dois pas faire ça ici, je le fais pour aller plus vite pour moi
        #t0=time.clock()
#        if not self.of==None:
#        
#            for d in new_data_set:
#                self.of.write("%.4f\t"%d),
#            self.of.write("\n")
        
        
    
    def plot_data_array(self, channel_no,data_x, data_y=None, label_plot=None):
        print "plot_data_array"
        print data_x
        self.matplotlibWidget.set_data(channel_no, data_x, data_y)
        
    def set_linestyle(self, channel_no,linestyle):
        self.matplotlibWidget.set_linestyle(channel_no,linestyle)
      
        #self.matplotlibWidget.show()        
        
        #self.matplotlibWidget.axis.legend([p1], ["line 1"])
        
    def draw_circle(self):
        self.matplotlibWidget.draw_circle()
        
    def turn_grid_on(self):
        self.matplotlibWidget.turn_grid_on()
        
    def set_axix_scale(self, x_min, x_max, y_min, y_max):
        self.matplotlibWidget.axis_scale(x_min, x_max, y_min, y_max)
             

    def clear_plot(self, channel_no):
        self.matplotlibWidget.clear_data(channel_no) 
    
    def get_x(self):
        return self.matplotlibWidget.get_data()[0]
        
    def get_y(self):
        return self.matplotlibWidget.get_data()[1]
    
    def x_label(self,label):
        self.matplotlibWidget.change_x_axis_label(label)
            
    def y_label(self,label):
        self.matplotlibWidget.change_y_axis_label(label) 
        
    def draw_line_x(self,x_pos):
        self.matplotlibWidget.draw_line_x(x_pos)
        
    def draw_line_y(self,y_pos):
        self.matplotlibWidget.draw_line_y(y_pos) 
  
        
class FancyScopeScreen(QtGui.QMainWindow, ui_PlotDisplayWindow.Ui_PlotDisplayWindow):
    def __init__(self, parent=None, data_array=np.array([]),name="Le nom que tu veux",channel_number=8,ofname=None,track_signal=True,channel_controls=chan_contr):
        # run the initializer of the class inherited from
        super(FancyScopeScreen, self).__init__()
                     
             #store the choice of channel controls parameters
        self.channel_controls=channel_controls
        
        self.color_set=color_blind_friendly_colors(channel_number)
        # this is where most of the GUI is made
        self.setupUi(self,self.channel_controls)
        self.customizeUi(channel_number)

        #Create an instance of auto-hiding widget which will contain the channel controls
        autoHide =  QtTools.QAutoHideDockWidgets(QtCore.Qt.RightDockWidgetArea, self) 
        
        # axes and figure initialization - short names for convenience   
        self.fig = self.mplwidget.figure
        self.setWindowTitle(name)
        self.ax = self.mplwidget.axes

        self.ax.xaxis.set_major_formatter(ticker.ScalarFormatter(useOffset = False))
        self.major_locator=self.ax.xaxis.get_major_locator()
        self.ax.yaxis.set_major_formatter(ticker.ScalarFormatter(useOffset = False))
  
        self.fig.canvas.draw()              
        self.toolbar = NavigationToolbar(self.fig.canvas, self)
        self.gridLayout_2.addWidget(self.toolbar,0, 0, 1, 1)
        # this is for a feature that doesn't exist yet
        self.history_length = 0

        self.num_channels = 0
        self.left_lines = [] 
        self.right_lines = [] 
        
        #Fills self.lineEdit_Name = [], self.comboBox_Type = [], self.comboBox_Instr = []. self.comboBox_Param = []
        #Whenever connect(obbject,SIGNAL(),function) is used it will call the function whenever the object is manipulated or something emits the same SIGNAL()
        for i in range (channel_number):   
            self.add_channel_controls()
        
        # objects to hold line data. Plot empty lists just to make handles
        # for line objects of the correct color   


        # create data_array attribute and use channel 0 as X by default
        self.data_array = data_array
        self.chan_X = 0
        self.time_Xaxis=False
        self.date_txt=self.fig.text(0.03,0.95,"",fontsize=15)

        
    def customizeUi(self, default_channels):       
       
       
        #this will be a dictionnary with the same keys as self.channel_controls corresponding to list
        #of 'N_channel' control Qwidgets.
        self.channel_objects={}
        for name,item in self.channel_controls.items():
            self.channel_objects[name]=[]
        
        # create a layout within the blank "plot_holder" widget and put the 
        # custom matplotlib zoom widget inside it. This way it expands to fill
        # the space, and we don't need to customize the ui_recordsweep.py file
        self.gridLayout_2 = QtGui.QGridLayout(self.plot_holder)
        self.gridLayout_2.setMargin(0)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        
        self.mplwidget = MatplotlibZoomWidget(self.plot_holder)
        self.mplwidget.setObjectName(_fromUtf8("mplwidget")) 
        self.gridLayout_2.addWidget(self.mplwidget, 1, 0, 1, 1)
   
  

    
    def add_channel_controls (self):
        """
        create an instance of each of the channel control objects for a new channel, assign the settings and link to the callback function.\n
        It also create an empty line for each axis.
        """
        #index of boxes to create
        i = self.num_channels
        
        self.num_channels = self.num_channels + 1    

        pos_LE = lambda x: 20 * (x + 1)
        
        line1, = self.ax.plot([], [])     
        
        for name,item in self.channel_controls.items():
            if item[1]=="radioButton":
                self.channel_objects[name].append(QtGui.QRadioButton(self.groupBoxes[name]))
                self.channel_objects[name][i].setText(_fromUtf8(""))
                self.connect(self.channel_objects[name][i], QtCore.SIGNAL("toggled(bool)"),self.XRadioButtonHandler)  
            elif item[1]=="checkBox":
                self.channel_objects[name].append(QtGui.QCheckBox(self.groupBoxes[name]))
                self.channel_objects[name][i].setText(_fromUtf8(""))
                self.connect(self.channel_objects[name][i], QtCore.SIGNAL("stateChanged(int)"), self.YCheckBoxHandler)
            elif item[1]=="comboBox":
                self.channel_objects[name].append(QtGui.QComboBox(self.groupBoxes[name]))
                if get_groupBox_purpouse(name)=="marker":
                    cbb_list=marker_set
                elif get_groupBox_purpouse(name)=="line":
                    cbb_list=line_set
                self.channel_objects[name][i].addItems(cbb_list)
#                self.channel_objects[name][i].setStyleSheet ("QComboBox::drop-down {border-width: 0px;} QComboBox::down-arrow {image: url(noimg); border-width: 0px;}")
                self.channel_objects[name][i].setMaxVisibleItems(len(cbb_list))
                self.connect(self.channel_objects[name][i], QtCore.SIGNAL("currentIndexChanged(int)"), self.ComboBoxHandler)
            elif item[1]=="lineEdit":
                self.channel_objects[name].append(QtGui.QLineEdit(self.groupBoxes[name]))
                self.channel_objects[name][i].setText(QtGui.QApplication.translate("RecordSweepWindow", "", None, QtGui.QApplication.UnicodeUTF8))
                self.connect(self.channel_objects[name][i], QtCore.SIGNAL("textEdited(QString)"), self.lineEditHandler)
            elif item[1]=="colorButton":
                self.channel_objects[name].append(QtGui.QPushButton(self.groupBoxes[name]))               
                color=self.color_set[np.mod(i,len(self.color_set))]
                line1.set_color(color)
                self.channel_objects[name][i].setStyleSheet('QPushButton {background-color: %s}'%color)
                self.channel_objects[name][i].setFixedSize(15,15)
                self.connect(self.channel_objects[name][i], QtCore.SIGNAL("clicked()"),self.colorButtonHandler) 
            
            self.channel_objects[name][i].setObjectName(_fromUtf8(name + "#" + str(i)))
            self.channel_objects[name][i].setGeometry(QtCore.QRect(7, 20*(i+1), 16, 16))
            if item[1]=="lineEdit":
                self.channel_objects[name][i].setGeometry(QtCore.QRect(10, pos_LE(i), 81, 16))
            elif item[1]=="comboBox" :
                self.channel_objects[name][i].setGeometry(QtCore.QRect(7, 20*(i+1), 32, 16))
            self.channel_objects[name][i].show()
#        self.radio
        #create line objects and append them to self.ax[R].lines autoatically

        
        
    def XRadioButtonHandler(self):
#        print "X clicked"  
        obj=self.sender()         
        name=obj.objectName()
        name=str(name.split("#")[0])
 
        for num, box in enumerate(self.channel_objects[name]):
            if box.isChecked():
                self.chan_X = num
                label=self.channel_objects["groupBox_Name"][num].text()
                self.ax.set_xlabel(label)
                if label=="time(s)":
                    self.time_Xaxis=True

                    hfmt=self.set_axis_time(want_format=True)
                    self.ax.xaxis.set_major_formatter(hfmt)
                    
                    
                    major_ticks=self.ax.xaxis.get_major_ticks()
#                 
                    for i,tick in enumerate(major_ticks):
                        if i==1:
                            n=tick.label.get_text()
                            label_i=n.split(" ")[0]
                        tick.label.set_rotation('vertical')
                    self.date_txt.set_text("Date :"+label_i)
                else:
                    self.time_Xaxis=False
                    self.date_txt.set_text("")
                    self.ax.xaxis.set_major_locator(self.major_locator)
                    self.ax.xaxis.set_major_formatter(ticker.ScalarFormatter(useOffset = False))
                    
                    for tick in self.ax.xaxis.get_major_ticks():
                        tick.label.set_rotation('horizontal')
        self.update_plot() 
        
    def YCheckBoxHandler(self):  
        """Update which data is used for the Y axis (both left and right)"""
        tot_label = ""
#        print "Y clicked"     
        obj=self.sender()         
        name=obj.objectName()
        name=str(name.split("#")[0])

        for num, box in enumerate(self.channel_objects[name]):

            if box.isChecked():
                label = str(self.channel_objects["groupBox_Name"][num].text())
                #unit = self.UNITS[str(self.comboBox_Param[num].currentText())]
                tot_label = tot_label + ", " + label #+ " (" + unit + ")" + ", "
                
        if get_groupBox_purpouse(name)=="Y":
                    self.ax.set_ylabel(tot_label.lstrip(', '))
        
        self.update_plot()             


    def ComboBoxHandler(self,num):
        obj=self.sender()         
        name=obj.objectName()
        
        name,idx=name.split("#")
        name=str(name)
        idx=int(idx)
        
        if get_groupBox_purpouse(name)=="marker":
            self.set_marker(idx,str(obj.currentText()))
        elif get_groupBox_purpouse(name)=="line":
            self.set_linestyle(idx,str(obj.currentText()))
            
            
            
    def colorButtonHandler(self):
        obj=self.sender()         
        name=obj.objectName()
        
        name,idx=name.split("#")
        name=str(name)
        idx=int(idx)

        color = QtGui.QColorDialog.getColor(initial = obj.palette().color(1))
        obj.setStyleSheet('QPushButton {background-color: %s}'%color.name())
#        btn.palette().color(1).name()
        print color.name()
        self.set_color(idx,str(color.name()))
    
    def lineEditHandler(self,mystr):
        obj=self.sender()         
        name=obj.objectName()
        name,idx=name.split("#")
        name=str(name)
        idx=int(idx)
        
        if self.channel_objects["groupBox_X"][idx].isChecked():
            self.ax.set_xlabel(self.channel_objects[name][idx].text())
            self.update_plot()


    def set_axis_ticks(self,ticks):
        if not len(ticks)==2:
            print "some ticks are missing, you should have ticks for X, and Y axes"
        else:
            for t in ticks[1]:
                self.channel_objects["groupBox_Y"][t].setCheckState(True)
#                print "Y",str(self.lineEdit_Name[t].text())
                
            self.channel_objects["groupBox_X"][ticks[0]].setChecked(True)
#            print "X",str(self.lineEdit_Name[ticks[0]].text())
            
    
    def get_X_axis_label(self):
        """Update which data is used for the X axis"""
        for num, box in enumerate(self.channel_objects["groupBox_X"]):
            if box.isChecked():
                label=str(self.channel_objects["groupBox_Name"][num].text())
        #getting rid of the eventual units
        if label.find('(')==-1:
            pass
        else:
            label=label[0:label.rfind('(')]
        return label
        
        
    def get_Y_axis_labels(self):  
        """Update which data is used for the Y axis (both left and right)"""
        labels = []
        
        for num, box in enumerate(self.channel_objects["groupBox_Y"]):
            if box.isChecked():
                label=str(self.channel_objects["groupBox_Name"][num].text())
                label=label[0:label.rfind('(')]
                labels.append(label)
        return labels
    
    def get_X_axis_index(self):
        """Update which data is used for the X axis"""
        index=0
        for num, box in enumerate(self.channel_objects["groupBox_X"]):
            if box.isChecked():
                index=num
        return index
        
        
    def get_Y_axis_index(self):  
        """Update which data is used for the Y axis (both left and right)"""
        index = 0
        for num, box in enumerate(self.channel_objects["groupBox_Y"]):
            if box.isChecked():
                index=num
        return index
    


    def convert_timestamp(self, timestamp):
        dts = map(datetime.datetime.fromtimestamp, timestamp)
        return dates.date2num(dts) # converted        

    def set_axis_time(self,want_format=False):

        if want_format:
            time_interval=self.data_array[-1,self.chan_X]-self.data_array[0,self.chan_X]
            if time_interval<500:
                hfmt = dates.DateFormatter('%m/%d %H:%M:%S')
            else:
                hfmt = dates.DateFormatter('%m/%d %H:%M')
            return hfmt
        else:
            time_data = self.convert_timestamp(self.data_array[:,self.chan_X])
            return time_data
        

    def set_marker(self,idx,marker):
        if idx < len(self.ax.lines):
            self.ax.lines[idx].set_marker(marker)
        self.mplwidget.rescale_and_draw() 
        
    def set_linestyle(self,idx,linesty):
        if idx < len(self.ax.lines):
            self.ax.lines[idx].set_linestyle(linesty)
        self.mplwidget.rescale_and_draw() 
        

    def set_color(self,idx,color):
        if idx < len(self.ax.lines):
            self.ax.lines[idx].set_color(color)
        self.mplwidget.rescale_and_draw()              
        

    def update_markers(self, marker_list):
        for idx, m in enumerate(marker_list):
            if idx < len(self.ax.lines):
                self.ax.lines[idx].set_marker(m)
        self.mplwidget.rescale_and_draw() 

    def update_colors(self, color_list):
        for idx, color in enumerate(color_list):
            if idx < len(self.ax.lines):
                self.ax.lines[idx].set_color(color)
        self.mplwidget.rescale_and_draw()                 

    def update_labels(self, label_list):
        for idx, label_text in enumerate(label_list):
            if idx == len(self.channel_objects["groupBox_Name"]):
                self.add_channel_controls()
                
            self.channel_objects["groupBox_Name"][idx].setText(label_text)
        
    def set_axis_scale(self, x_min, x_max, y_min, y_max):
        self.ax.axis([x_min, x_max,y_min, y_max])
        self.mplwidget.rescale_and_draw()     

    def clear_plot(self, channel_no):
        #self.matplotlibWidget.clear_data(channel_no) 
        self.ax.lines[channel_no].set_data([],[])
        self.mplwidget.rescale_and_draw()
    
    def plot_data_array(self, channel_no, data_x, data_y=None):
        if data_y == None:
            data_y= data_x
            data_x=np.arange(len(data_y))
        print data_x
        print data_y
        self.ax.lines[channel_no].set_data(data_x,data_y)
            
        self.mplwidget.rescale_and_draw()            
            
    def update_plot(self, data_array = None): 
#        print data_array
        #print("are we getting there")
        if not data_array == None:
            self.data_array=data_array
            
        if not self.data_array==np.array([]):
#        ncols=np.size(self.data_array,0)
            nlines=np.size(self.data_array,0)
            # if the number of columns is more than the number of control boxes
    #            while self.num_channels < self.data_array.shape[1]:
    #                self.add_channel_controls()
                
    
            xdata=None
            ydata=None
            #print nlines
            #print self.num_channels
            #if there is an extra row it means it is the x axis
            if nlines == self.num_channels+1:
                #print "xdata"
                xdata = self.data_array[0,:]         
    
    #        print xdata
            for chan_id in range(self.num_channels):
           

                if self.channel_objects["groupBox_Y"][chan_id].isChecked():
                    print "passed %i"%(chan_id)
                    if nlines == self.num_channels:
                        xdata=self.data_array[chan_id,:]
                        ydata=None
                    else:
                        #in this case the first row is the x axis
                        ydata=self.data_array[chan_id+1,:]
                    
    #                print xdata
    #                print ydata
                    self.mplwidget.set_data(chan_id,xdata, ydata,label=self.channel_objects["groupBox_Name"][chan_id].text())
                else:
                    self.mplwidget.set_data(chan_id,[], [])
                
                
                    #look which checkbox is checked and plot corresponding data    
            #call a method defined in the module mplZoomwidget.py         
        self.mplwidget.rescale_and_draw() 



    def update_fit(self,data_array = None):
#        print "PDW.update_fit"
        if not data_array == None:
            # if the number of columns is more than the number of control boxes
#            print "chan num", self.num_channels
#            print len(self.ax.lines)
            if self.num_channels == len(self.ax.lines):
                line, = self.ax.plot([],[])
#                print line
                
            if data_array.size > 0:
#                print "updating"
                xdata = data_array[0]    
                ydata = data_array[1]
                self.ax.lines[-1].set_data(xdata, ydata)
#                print len(self.ax.lines)
#                print self.ax.lines[-1]
#                self.ax.lines[-1].set_color('or')
                                     
            #call a method defined in the module mplZoomwidget.py         
            self.mplwidget.rescale_and_draw() 
        
    def change_x_axis_label(self, new_label_x):
        
        self.mplwidget.change_xlabel(new_label_x)
#        .xlabel(new_label_x)
    
    def change_y_axis_label(self, new_label_y):
        
        self.mplwidget.change_ylabel(new_label_y)
            
    def remove_fit(self):
        self.ax.lines[-1].set_data([], [])

def convert_timestamp(timestamp):
    dts = map(datetime.datetime.fromtimestamp, timestamp)
    return dates.date2num(dts) # converted    
        
if __name__ == "__main__":


    app = QtGui.QApplication(sys.argv)
    app.setApplicationName('MyWindow')

    trig_off=np.ones(20)
    trig_on=np.ones(5)*5
    trig_signal=trig_off
    for i in range(3):
        trig_signal=np.append(trig_signal,trig_on)
        trig_signal=np.append(trig_signal,trig_off)
#    print "trigger signal"
#    print trig_signal
#    print
    
    chan_num=2    
    
    raw_signal=np.transpose(np.vstack([np.ones(chan_num) for a in range(len(trig_signal))]))
#    print np.size(raw_signal,0)
#    main = FancyScopeScreen(channel_number=chan_num)
    main = ScopeScreen(channel_number=chan_num)
    main.plot_data_array(0,trig_signal)
    print main.get_x()
##    main.clear_plot(0)
##    main.plot_data_array(0,[1,2,3,4],[1,2,3,4])
#    main.update_plot(raw_signal)
##    for i in range (8):   
##        main.add_channel_controls()
    #labels=["Pomme","Banane","Orange","Kiwi"]
    #main.resize(666, 333)
    main.show()

    sys.exit(app.exec_())