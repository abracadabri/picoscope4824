# -*- coding: utf-8 -*-
"""
Created on Tue Nov 11 20:28:48 2014

@author: Helene
"""

from readconfigfile import get_config_setting

def get_voltage_range():
    return int(get_config_setting("VRANGE", "settings.txt"))

def get_sampling_interval():
    return float(get_config_setting("SAMPLING_INTERVAL","settings.txt"))
    
def get_obs_duration():
    return float(get_config_setting("OBS_DURATION", "settings.txt"))
       
